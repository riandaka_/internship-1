<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Raport_model_user extends CI_Model
{
    // private $_table = "";

    public $workbook_id;
    public $stage_id;
    public $modul_id;
    public $employee_id;
    public $employee_name;
    public $status;
    public $image = "default.jpg";

    public function getAll()
    {
        $id = $this->session->userdata('employee_id');
        $this->db->select('assessment.*, account.employee_name as yaww');
        // $this->db->join('ruangkerja', 'assessment.employee_id = ruangkerja.employee_id');
        $this->db->join('account', 'assessment.employee_id = account.employee_id');
        $this->db->from('assessment');
        $this->db->where('assessment.employee_id', $id);
        return $this->db->get()->result();
    }

    public function getById($id)
    {
        $this->db->select('workbook.*, account.employee_name as yaww');
        $this->db->join('account', 'workbook.employee_id = account.employee_id');
        $this->db->from('workbook');
        $this->db->where('workbook_id', $id);
        return $this->db->get()->row();
    }

        //fungsi cek level
        function is_role()
        {
            return $this->session->userdata('role');
        }
}