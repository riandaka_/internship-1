<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Workbook_model_user extends CI_Model
{
    private $_table = "workbook";

    public $workbook_id;
    public $stage_id;
    public $modul_id;
    public $employee_id;
    public $employee_name;
    public $status;
    public $image = "default.jpg";

    public function rules()
    {
        return [

            ['field' => 'stage_id',
            'rules'  => 'required'],

            ['field' => 'modul_id',
            'rules'  => 'required'],

            ['field' => 'employee_id',
            'rules'  => 'required'],

            ['field' => 'employee_name',
            'rules'  => 'required'],

            ['field' => 'progress',
            'rules'  => 'required'],

            ['field' => 'status',
             'rules' => 'required'],

            ['field' => 'image',
            'rules' => 'required'],
        ];
    }

        //fungsi cek level
        function is_role()
        {
            return $this->session->userdata('role');
        }

    public function getAll()
    {
        $id = $this->session->userdata('employee_id');
        $this->db->select('workbook.*, account.employee_name as yaww');
        $this->db->join('account', 'workbook.employee_id = account.employee_id');
        $this->db->from('workbook');
        $this->db->where('workbook.employee_id', $id);
        // print_r($id); die;
        return $this->db->get()->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["workbook_id" => $id])->row();
    }

    public function get_data_stage()
    {
        $query = $this->db->get('stage');
        return $query;
    }
    
    public function get_data_modul($id)
    {
        $hasil = $this->db->query("SELECT * FROM modul WHERE stage_id = $id");
        return $hasil->result();
    }

    public function save()
    {
        // $post = $this->input->post();
        // print_r($post); die;
        // $this->workbook_id = $post["workbook_id"];
        $data = array (
        "stage_id" => $this->input->post("stage_id"),
        "modul_id" => $this->input->post("modul_id"),
        "employee_id" => $this->session->userdata('employee_id'),
        "progress" => $this->input->post("progress"),
        "status" => $this->input->post("status"),
        "image" => $this->_uploadImage()
        );
        // print_r($data);die;

        $this->db->insert("workbook", $data); 
    }
    
    public function update($id, $data)
    {
        $this->db->where('workbook_id',$id);
        $this->db->update("workbook", $data);
    }

    public function delete($id)
    {
        $this->_deleteImage($id);
        return $this->db->delete("workbook", array("workbook_id" => $id));
    }

    private function _uploadImage()
    {
        $config['upload_path']          = './upload/workbook/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['file_name']            = $this->workbook_id;
        $config['overwrite']			= true;
        $config['max_size']             = 10240; // 10MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('image')) {
            return $this->upload->data("file_name");
        }
    
        return "default.jpg";
    }

    private function _deleteImage($id)
    {
        $workbook = $this->getById($id);
            if ($workbook->image != "default.jpg") {
	        $filename = explode(".", $workbook->image)[0];
		return array_map('unlink', glob(FCPATH."upload/workbook/$filename.*"));
        }
    }
}

 