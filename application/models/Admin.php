<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Model
{
    //fungsi cek session logged in
    function is_logged_in()
    {
        return $this->session->userdata('id');
    }

    public function getChart1()
    {
      $q = $this->db->query("select status, count(status) as jml from workbook group by status");
      return $q->result_array();
    }

    public function jumlah_peserta(){
         return $this->db->query("SELECT COUNT(*) AS jumlah FROM account WHERE role = 'user'")->result();
    }

    public function jumlah_modul(){
         return $this->db->query("SELECT COUNT(*) AS jamleh FROM modul")->result();
    }

    //fungsi cek level
    function is_role()
    {
        return $this->session->userdata('role');
    }

    //fungsi check login
    function check_login($table, $field1, $field2)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($field1);
        $this->db->where($field2);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return FALSE;
        } else {
            return $query->result();
        }
    }
}