<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ruangkerja_model extends CI_Model
{
    private $_table = "ruangkerja";

    public $ruangkerja_id;
    public $employee_id;
    public $employee_name;
    public $user_email;
    public $start_time;
    public $posttest_score;
    public $status;
    public $deadline_course;
    public $clear_time;
    public $directorate;

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
        // print_r('tes'); die;
    }

    // Fungsi untuk melakukan proses upload file
    public function upload_file($filename){
        $this->load->library('upload'); // Load librari upload
        
        $config['upload_path'] = './excel/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size'] = '2048';
        $config['overwrite'] = true;
        $config['file_name'] = $filename;
    
        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }
    
    public function autoIncrementRuangkerja(){
        return $this->db->query("ALTER TABLE ruangkerja AUTO_INCREMENT =1;");
    }

    public function importExcel($data)
    {
        return $this->db->insert_batch('ruangkerja', $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["ruangkerja_id" => $id])->row();
    }

    public function get_data_stage()
    {
        $query = $this->db->get('stage');
        return $query;
    }

    public function save()
    {

                // $post = $this->input->post();
        // print_r($post); die;
        // $this->workbook_id = $post["workbook_id"];
        $data = array (
        "ruangkerja_id" => $this->input->post("ruangkerja_id"),
        "employee_id" => $this->input->post("employee_id"),
        "employee_name" => $this->input->post("employee_name"),
        "user_email" => $this->input->post("user_email"),
        "start_time" => $this->input->post("start_time"),
        "posttest_score" => $this->input->post("posttest_score"),
        "status" => $this->input->post("status"),
        "deadline_course" => $this->input->post("deadline_course"),
        "clear_time" => $this->input->post("clear_time"),
        "directorate" => $this->input->post("directorate")
        );

        $this->db->insert("ruangkerja", $data); 
    }
    
    public function update($id, $data)
    {
        $this->db->where('ruangkerja_id',$id);
        $this->db->update("ruangkerja", $data);
    }

    public function delete($id)
    {
        // $this->_deleteImage($id);
        return $this->db->delete($this->_table, array("ruangkerja_id" => $id));
    }

        //fungsi cek level
        function is_role()
        {
            return $this->session->userdata('role');
        }

    
}

 