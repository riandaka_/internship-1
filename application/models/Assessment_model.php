<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Assessment_model extends CI_Model
{
    private $_table = "assessment";

    public $assessment_id;
    public $stage_id;
    public $modul_id;
    public $employee_id;
    public $nilai_definisi;
    public $nilai_tujuan;
    public $nilai_langkah;
    public $nilai_contoh;
    public $nilai_analisis;
    public $nilai_solusi;
    public $total_nilai;

    //fungsi cek level
    function is_role()
    {
        return $this->session->userdata('role');
    }

    public function getAll()
    {
        $this->db->select('assessment.*, account.employee_name as yaww');
        $this->db->join('account', 'assessment.employee_id = account.employee_id');
        $this->db->from('assessment');
        return $this->db->get()->result();
        // print_r('tes'); die;
    }

    public function getById($id)
    {
        $this->db->select('assessment.*, account.employee_name as yaww');
        $this->db->join('account', 'assessment.employee_id = account.employee_id');
        $this->db->from('assessment');
        $this->db->where('assessment_id', $id);
        return $this->db->get()->row();
    }

    public function get_data_stage()
    {
        $query = $this->db->get('stage');
        return $query;
    }
    
    public function get_data_modul($id)
    {
        $hasil = $this->db->query("SELECT * FROM modul WHERE stage_id = $id");
        return $hasil->result();
    }

    public function save()
    {

        $nilai_definisi = $this->input->post('nilai_definisi');
        $nilai_definisi *= 0.1;
        $nilai_tujuan = $this->input->post('nilai_tujuan');
        $nilai_tujuan *= 0.1;
        $nilai_langkah = $this->input->post('nilai_langkah');
        $nilai_langkah *= 0.2;
        $nilai_contoh = $this->input->post('nilai_contoh');
        $nilai_contoh *= 0.15;
        $nilai_analisis = $this->input->post('nilai_analisis');
        $nilai_analisis *= 0.2;
        $nilai_solusi = $this->input->post('nilai_solusi');
        $nilai_solusi *= 0.25;
        $total_nilai = $nilai_definisi + $nilai_tujuan + $nilai_langkah + $nilai_contoh + $nilai_analisis + $nilai_solusi;
        $status_lulus = $nilai_definisi + $nilai_tujuan + $nilai_langkah + $nilai_contoh + $nilai_analisis + $nilai_solusi;

        if($total_nilai >= 70){
            $status_lulus = "Lulus";
        }else{
            $status_lulus = "Tidak Lulus";
        }
                // $post = $this->input->post();
        // print_r($post); die;
        // $this->workbook_id = $post["workbook_id"];
        $data = array (
        "stage_id" => $this->input->post("stage_id"),
        "modul_id" => $this->input->post("modul_id"),
        "employee_id" => $this->input->post("employee_id"),
        "nilai_definisi" => $this->input->post("nilai_definisi"),
        "nilai_tujuan" => $this->input->post("nilai_tujuan"),
        "nilai_langkah" => $this->input->post("nilai_langkah"),
        "nilai_contoh" => $this->input->post("nilai_contoh"),
        "nilai_analisis" => $this->input->post("nilai_analisis"),
        "nilai_solusi" => $this->input->post("nilai_solusi"),
        "total_nilai" => $total_nilai,
        "status_lulus" => $status_lulus
        );

        $this->db->insert("assessment", $data); 
    }
    
    public function update($id, $data)
    {
        $this->db->where('assessment_id',$id);
        $this->db->update("assessment", $data);
    }

    public function delete($id)
    {
        // $this->_deleteImage($id);
        return $this->db->delete($this->_table, array("assessment_id" => $id));
    }

    
}

 