<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Account_model extends CI_Model
{
    private $_table = "account";

    public $employee_id;
    public $directorate;
    public $employee_name;
    public $username;
    public $password;
    public $user_email;
    public $role;

    public function rules()
    {
        return [
            ['field' => 'employee_id',
            'label'  => 'No. Pekerja',
            'rules'  => 'required'],

            ['field' => 'employee_name',
            'label'  => 'Nama Pekerja',
            'rules'  => 'required'],

            ['field' => 'username',
             'label' => 'username',
             'rules' => 'required'],

            ['field' => 'password',
             'label' => 'password',
             'rules' => 'required']
        ];
    }

    //fungsi cek level
    function is_role()
    {
        return $this->session->userdata('role');
    }

    public function getAll()
    {
        return $this->db->get_where($this->_table, array('role' => 'user'))->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["employee_id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->employee_id = $post["employee_id"];
        $this->directorate = $post["directorate"];
        $this->employee_name = $post["employee_name"];
        $this->username = $post["username"];
        $this->password = $post["password"];
        $this->user_email = $post["user_email"];
        $this->role = $post["role"];
        $this->db->insert($this->_table, $this);
    }
    
    public function update()
    {
        $post = $this->input->post();
        $this->employee_id = $post["employee_id"];
        $this->directorate = $post["directorate"];
        $this->employee_name = $post["employee_name"];
        $this->username = $post["username"];
        $this->password = $post["password"];
        $this->user_email = $post["user_email"];
        $this->role = $post["role"];$this->db->update($this->_table, $this, array('employee_id' => $post['employee_id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("employee_id" => $id));
    }
}

