<!--

=========================================================
* Argon Dashboard - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>
    EPPM Go!
  </title>
  <!-- Favicon -->
  <link href="<?php echo base_url() ?>assets/img/brand/favicon.png" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="<?php echo base_url() ?>assets/js/plugins/nucleo/css/nucleo.css" rel="stylesheet" />
  <link href="<?php echo base_url() ?>assets/js/plugins/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="<?php echo base_url() ?>assets/css/argon-dashboard.css?v=1.1.0" rel="stylesheet" />
</head>

<body class="">
  <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Brand -->
      <a class="navbar-brand pt-0" href="<?php echo base_url();?>index.php/admin">
        <img src="../assets/img/brand/eppmgo!.png" class="navbar-brand-img" alt="...">
      </a>
      <!-- User -->
      <ul class="nav align-items-center d-md-none">
        <li class="nav-item dropdown">
          <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="ni ni-bell-55"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="../assets/img/theme/team-4-800x800.jpg">
              </span>
            </div>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
            <div class=" dropdown-header noti-title">
              <h6 class="text-overflow m-0">Welcome!</h6>
            </div>
            <a href="./examples/profile.html" class="dropdown-item">
              <i class="ni ni-single-02"></i>
              <span>My profile</span>
            </a>
            <a href="./examples/profile.html" class="dropdown-item">
              <i class="ni ni-settings-gear-65"></i>
              <span>Settings</span>
            </a>
            <a href="./examples/profile.html" class="dropdown-item">
              <i class="ni ni-calendar-grid-58"></i>
              <span>Activity</span>
            </a>
            <a href="./examples/profile.html" class="dropdown-item">
              <i class="ni ni-support-16"></i>
              <span>Support</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#!" class="dropdown-item">
              <i class="ni ni-user-run"></i>
              <span>Logout</span>
            </a>
          </div>
        </li>
      </ul>
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Collapse header -->
        <div class="navbar-collapse-header d-md-none">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="<?php echo base_url();?>index.php/admin">
                <img src="../assets/img/brand/eppmgo!.png">
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <!-- Form -->
        <!-- <form class="mt-4 mb-3 d-md-none">
          <div class="input-group input-group-rounded input-group-merge">
            <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <span class="fa fa-search"></span>
              </div>
            </div>
          </div>
        </form> -->
        <!-- Navigation -->
        <ul class="navbar-nav">
          <li class="nav-item"  class="active" >
          <a class=" nav-link active " href="<?php echo base_url();?>index.php/admin"> <i class="ni ni-tv-2 text-primary"></i> Dashboard
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href=" <?php echo base_url(); ?>admin/account">
              <i class="ni ni-bullet-list-67 text-red"></i> Daftar Peserta
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href=" <?php echo base_url(); ?>admin/workbook">
              <i class="ni ni-bullet-list-67 text-red"></i> Workbook
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href=" <?php echo base_url(); ?>admin/assessment">
              <i class="ni ni-bullet-list-67 text-red"></i> Assessment
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href=" <?php echo base_url(); ?>admin/ruangkerja">
              <i class="ni ni-bullet-list-67 text-red"></i> Data Ruangkerja
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href=" <?php echo base_url(); ?>admin/raport">
              <i class="ni ni-bullet-list-67 text-red"></i> Raport
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url();?>index.php/login/Logout">
              <i class="ni ni-key-25 text-info"></i> Logout
            </a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="./examples/register.html">
              <i class="ni ni-circle-08 text-pink"></i> Register
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="./examples/icons.html">
              <i class="ni ni-planet text-blue"></i> Icons
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="./examples/maps.html">
              <i class="ni ni-pin-3 text-orange"></i> Maps
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="./examples/profile.html">
              <i class="ni ni-single-02 text-yellow"></i> User profile
            </a>
          </li> -->
        </ul>
        <!-- Divider -->
        <hr class="my-3">
        <!-- Heading -->
        <h6 class="navbar-heading text-muted">PT. Pertamina (Persero)</h6>
        <!-- Navigation -->
        <ul class="navbar-nav mb-md-3">
          <li class="nav-item">
            <a class="nav-link" href="https://www.pertamina.com">
              <i class="ni ni-spaceship"></i> Pertamina
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="ni ni-palette"></i> User-Guide
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="ni ni-ui-04"></i> User Agreement & Policy
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="<?php echo base_url();?>index.php/admin">Dashboard</a>
        <!-- Form -->
        <!-- <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form> -->
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="../assets/img/theme/team-4-800x800.jpg">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">Admin</span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0">Welcome!</h6>
              </div>
              <!-- <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>My profile</span>
              </a>
              <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-settings-gear-65"></i>
                <span>Settings</span>
              </a>
              <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-calendar-grid-58"></i>
                <span>Activity</span>
              </a>
              <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-support-16"></i>
                <span>Support</span>
              </a> -->
              <div class="dropdown-divider"></div>
              <a href="<?php echo base_url();?>index.php/login/Logout" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <!-- <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Traffic</h5>
                      <span class="h2 font-weight-bold mb-0">350,897</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                        <i class="fas fa-chart-bar"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div> -->
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Jumlah Modul</h5>
                      <span class="h2 font-weight-bold mb-0"><?php foreach ($jumlah_modul as $jumlah_modul) { ?>
                      <?= $jumlah_modul->jamleh ?><?php } ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                        <i class="fas fa-chart-pie"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                 <!--    <span class="text-danger mr-2"><i class="fas fa-arrow-down"></i> 3.48%</span> -->
                    <!-- <span class="text-nowrap">Since last week</span> -->
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Jumlah Peserta</h5>
                      <span class="h2 font-weight-bold mb-0"><?php foreach ($jumlah_peserta as $jumlah_peserta) { ?>
                      <?= $jumlah_peserta->jumlah ?><?php } ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                        <i class="fas fa-users"></i>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
            <!-- <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Performance</h5>
                      <span class="h2 font-weight-bold mb-0">49,65%</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="fas fa-percent"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fas fa-arrow-up"></i> 12%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid mt--7">
      <div class="row">
        <div class="col-xl-8 mb-5 mb-xl-0">
          <div class="card bg-gradient-default shadow">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-uppercase text-light ls-1 mb-1">Progres </h6>
                  <h2 class="text-white mb-0">Pengerjaan Workbook</h2>
                </div>
                <div class="col">
                  <ul class="nav nav-pills justify-content-end">
                    <!-- <li class="nav-item mr-2 mr-md-0" data-toggle="chart" data-target="#chart-sales" data-update='{"data":{"datasets":[{"data":[0, 30, 10, 30, 15, 40, 20, 60, 60]}]}}' data-prefix="" data-suffix="Orang">
                      <a href="#" class="nav-link py-2 px-3 active" data-toggle="tab">
                        <span class="d-none d-md-block">Month</span>
                        <span class="d-md-none">M</span>
                      </a>
                    </li> -->
                    <!-- <li class="nav-item" data-toggle="chart" data-target="#chart-sales" data-update='{"data":{"datasets":[{"data":[0, 20, 5, 25, 10, 30, 15, 40, 40]}]}}' data-prefix="" data-suffix="Orang">
                      <a href="#" class="nav-link py-2 px-3" data-toggle="tab">
                        <span class="d-none d-md-block">Week</span>
                        <span class="d-md-none">W</span>
                      </a>
                    </li> -->
                  </ul>
                </div>
              </div>
            </div>
            <div class="card-body">
              <!-- Chart -->
              <div class="chart">
                <!-- Chart wrapper -->
                <canvas id="chart-sales" class="chart-canvas"></canvas>
              </div>
            </div>
          </div>
        </div>
        <!-- <div class="col-xl-4">
          <div class="card shadow">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
                <div class="col">
                  <h6 class="text-uppercase text-muted ls-1 mb-1">Performance</h6>
                  <h2 class="mb-0">Total orders</h2>
                </div>
              </div>
            </div>
            <div class="card-body"> -->
              <!-- Chart -->
              <div class="chart">
                <canvas id="chart-orders" class="chart-canvas"></canvas>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <!-- Footer -->
      <footer class="footer">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-xl-6">
            <!-- <div class="copyright text-center text-xl-left text-muted">
              &copy; 2019 <a href="https://www.pertamina.com" class="font-weight-bold ml-1" target="_blank">PT. Pertamina</a>
            </div>
          </div>
          <div class="col-xl-6">
            <ul class="nav nav-footer justify-content-center justify-content-xl-end">
              <li class="nav-item">
                <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
              </li>
            </ul>
          </div> -->
        </div>
      </footer>
    </div>
  </div>
  <!--   Core   -->
  <script src="<?php echo base_url() ?>assets/js/plugins/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <!--   Optional JS   -->
  <script src="<?php echo base_url() ?>assets/js/plugins/chart.js/dist/Chart.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/plugins/chart.js/dist/Chart.extension.js"></script>
  <!--   Argon JS   -->
  <!-- <script src="<?php echo base_url() ?>assets/js/argon-dashboard.min.js?v=1.1.0"></script>
  <script src="<?php echo base_url() ?>assets/js/argon-dashboard.js"></script> -->
  <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
  <script>
    window.TrackJS &&
      TrackJS.install({
        token: "ee6fab19c5a04ac1a32a645abde4613a",
        application: "argon-dashboard-free"
      });
  </script>
<script>
	$(document).ready(function () {
		$.get("http://localhost:8080/eppm-go!/index.php/Chart", function (data, status) {
				console.log(data, status)
			if (status) {
        var data = JSON.parse(data)
				labels = []
				jml = []
				data.forEach(element => {
					var statusdata = element.status
					var jmldata = element.jml
					labels.push(statusdata)
					jml.push(jmldata)
        });
        var Charts = (function() {

// Variable

var $toggle = $('[data-toggle="chart"]');
var mode = 'light'; //(themeMode) ? themeMode : 'light';
var fonts = {
  base: 'Open Sans'
}

// Colors
var colors = {
  gray: {
    100: '#f6f9fc',
    200: '#e9ecef',
    300: '#dee2e6',
    400: '#ced4da',
    500: '#adb5bd',
    600: '#8898aa',
    700: '#525f7f',
    800: '#32325d',
    900: '#212529'
  },
  //warna yg bisa dipakai
  theme: {
    'default': '#172b4d',
    'primary': '#5e72e4',
    'secondary': '#f4f5f7',
    'info': '#11cdef',
    'success': '#2dce89',
    'danger': '#f5365c',
    'warning': '#fb6340',
    'custom' : '#f5223e'
  },
  black: '#12263F',
  white: '#FFFFFF',
  transparent: 'transparent',
};


// Methods

// Chart.js global options
function chartOptions() {

  // Options
  var options = {
    defaults: {
      global: {
        responsive: true,
        maintainAspectRatio: false,
        defaultColor: (mode == 'dark') ? colors.gray[700] : colors.gray[600],
        defaultFontColor: (mode == 'dark') ? colors.gray[700] : colors.gray[600],
        defaultFontFamily: fonts.base,
        defaultFontSize: 13,
        layout: {
          padding: 0
        },
        legend: {
          display: false, //Pilih true buat memunculkan legend di bawah chart
          position: 'bottom',
          labels: {
            usePointStyle: true,
            padding: 16
          }
        },
        elements: { //elements we use for the chart, you can choose between one of those
          point: {
            radius: 0,
            backgroundColor: colors.theme['primary']
          },
          line: {
            tension: .4,
            borderWidth: 4,
            borderColor: colors.theme['primary'],
            backgroundColor: colors.transparent,
            borderCapStyle: 'rounded'
          },
          rectangle: {
            backgroundColor: colors.theme["warning"]
          },
          arc: {
            backgroundColor: colors.theme['primary'],
            borderColor: (mode == 'dark') ? colors.gray[800] : colors.white,
            borderWidth: 4
          }
        },
        tooltips: {
          enabled: false,
          mode: 'index',
          intersect: false,
          custom: function(model) {

            // Get tooltip
            var $tooltip = $('#chart-tooltip');

            // Create tooltip on first render
            if (!$tooltip.length) {
              $tooltip = $('<div id="chart-tooltip" class="popover bs-popover-top" role="tooltip"></div>');

              // Append to body
              $('body').append($tooltip);
            }

            // Hide if no tooltip
            if (model.opacity === 0) {
              $tooltip.css('display', 'none');
              return;
            }

            function getBody(bodyItem) {
              return bodyItem.lines;
            }

            // Fill with content
            if (model.body) {
              var titleLines = model.title || [];
              var bodyLines = model.body.map(getBody);
              var html = '';

              // Add arrow
              html += '<div class="arrow"></div>';

              // Add header
              titleLines.forEach(function(title) {
                html += '<h3 class="popover-header text-center">' + title + '</h3>';
              });

              // Add body
              bodyLines.forEach(function(body, i) {
                var colors = model.labelColors[i];
                var styles = 'background-color: ' + colors.backgroundColor;
                var indicator = '<span class="badge badge-dot"><i class="bg-primary"></i></span>';
                var align = (bodyLines.length > 1) ? 'justify-content-left' : 'justify-content-center';
                html += '<div class="popover-body d-flex align-items-center ' + align + '">' + indicator + body + '</div>';
              });

              $tooltip.html(html);
            }

            // Get tooltip position
            var $canvas = $(this._chart.canvas);

            var canvasWidth = $canvas.outerWidth();
            var canvasHeight = $canvas.outerHeight();

            var canvasTop = $canvas.offset().top;
            var canvasLeft = $canvas.offset().left;

            var tooltipWidth = $tooltip.outerWidth();
            var tooltipHeight = $tooltip.outerHeight();

            var top = canvasTop + model.caretY - tooltipHeight - 16;
            var left = canvasLeft + model.caretX - tooltipWidth / 2;

            // Display tooltip
            $tooltip.css({
              'top': top + 'px',
              'left': left + 'px',
              'display': 'block',
              'z-index': '100'
            });

          },
          callbacks: {
            label: function(item, data) {
              var label = data.datasets[item.datasetIndex].label || '';
              var yLabel = item.yLabel;
              var content = '';

              if (data.datasets.length > 1) {
                content += '<span class="badge badge-primary mr-auto">' + label + '</span>';
              }

              content += '<span class="popover-body-value">' + yLabel + '</span>';
              return content;
            }
          }
        }
      },
      doughnut: {
        cutoutPercentage: 83,
        tooltips: {
          callbacks: {
            title: function(item, data) {
              var title = data.labels[item[0].index];
              return title;
            },
            label: function(item, data) {
              var value = data.datasets[0].data[item.index];
              var content = '';

              content += '<span class="popover-body-value">' + value + '</span>';
              return content;
            }
          }
        },
        legendCallback: function(chart) {
          var data = chart.data;
          var content = '';

          data.labels.forEach(function(label, index) {
            var bgColor = data.datasets[0].backgroundColor[index];

            content += '<span class="chart-legend-item">';
            content += '<i class="chart-legend-indicator" style="background-color: ' + bgColor + '"></i>';
            content += label;
            content += '</span>';
          });

          return content;
        }
      }
    }
  }

  // yAxes
  Chart.scaleService.updateScaleDefaults('linear', {
    gridLines: {
      borderDash: [2],
      borderDashOffset: [2],
      color: (mode == 'dark') ? colors.gray[900] : colors.gray[300],
      drawBorder: false,
      drawTicks: false,
      lineWidth: 0,
      zeroLineWidth: 0,
      zeroLineColor: (mode == 'dark') ? colors.gray[900] : colors.gray[300],
      zeroLineBorderDash: [2],
      zeroLineBorderDashOffset: [2]
    },
    ticks: {
      beginAtZero: true,
      padding: 10,
      callback: function(value) {
        if (!(value % 10)) {
          return value
        }
      }
    }
  });

  // xAxes
  Chart.scaleService.updateScaleDefaults('category', {
    gridLines: {
      drawBorder: false,
      drawOnChartArea: false,
      drawTicks: false
    },
    ticks: {
      padding: 20
    },
    maxBarThickness: 25   //mengatur tebalnya bar chart
  });

  return options;

}

// Parse global options
function parseOptions(parent, options) {
  for (var item in options) {
    if (typeof options[item] !== 'object') {
      parent[item] = options[item];
    } else {
      parseOptions(parent[item], options[item]);
    }
  }
}

// Push options
function pushOptions(parent, options) {
  for (var item in options) {
    if (Array.isArray(options[item])) {
      options[item].forEach(function(data) {
        parent[item].push(data);
      });
    } else {
      pushOptions(parent[item], options[item]);
    }
  }
}

// Pop options
function popOptions(parent, options) {
  for (var item in options) {
    if (Array.isArray(options[item])) {
      options[item].forEach(function(data) {
        parent[item].pop();
      });
    } else {
      popOptions(parent[item], options[item]);
    }
  }
}

// Toggle options
function toggleOptions(elem) {
  var options = elem.data('add');
  var $target = $(elem.data('target'));
  var $chart = $target.data('chart');

  if (elem.is(':checked')) {

    // Add options
    pushOptions($chart, options);

    // Update chart
    $chart.update();
  } else {

    // Remove options
    popOptions($chart, options);

    // Update chart
    $chart.update();
  }
}

// Update options
function updateOptions(elem) {
  var options = elem.data('update');
  var $target = $(elem.data('target'));
  var $chart = $target.data('chart');

  // Parse options
  parseOptions($chart, options);

  // Toggle ticks
  toggleTicks(elem, $chart);

  // Update chart
  $chart.update();
}

// Toggle ticks
function toggleTicks(elem, $chart) {

  if (elem.data('prefix') !== undefined || elem.data('prefix') !== undefined) {
    var prefix = elem.data('prefix') ? elem.data('prefix') : '';
    var suffix = elem.data('suffix') ? elem.data('suffix') : '';

    // Update ticks
    $chart.options.scales.yAxes[0].ticks.callback = function(value) {
      if (!(value % 10)) {
        return prefix + value + suffix;
      }
    }

    // Update tooltips
    $chart.options.tooltips.callbacks.label = function(item, data) {
      var label = data.datasets[item.datasetIndex].label || '';
      var yLabel = item.yLabel;
      var content = '';

      if (data.datasets.length > 1) {
        content += '<span class="popover-body-label mr-auto">' + label + '</span>';
      }

      content += '<span class="popover-body-value">' + prefix + yLabel + suffix + '</span>';
      return content;
    }

  }
}


// Events

// Parse global options
if (window.Chart) {
  parseOptions(Chart, chartOptions());
}

// Toggle options
$toggle.on({
  'change': function() {
    var $this = $(this);

    if ($this.is('[data-add]')) {
      toggleOptions($this);
    }
  },
  'click': function() {
    var $this = $(this);

    if ($this.is('[data-update]')) {
      updateOptions($this);
    }
  }
});


// Return

return {
  colors: colors,
  fonts: fonts,
  mode: mode
};

})();
				var SalesChart = (function () {

					// Variables

					var $chart = $('#chart-sales');

					function init($chart) {

						var salesChart = new Chart($chart, {
							type: 'bar',  //jenis chart
							options: {
								scales: {
									yAxes: [{
										gridLines: {
											lineWidth: 1,
											color: Charts.colors.gray[900],
											zeroLineColor: Charts.colors.gray[900]
										},
										ticks: {
											callback: function (value) {
												if (!(value % 1)) {       //Label di sumbu y, ganti 1 dengan angka lain
													return '' + value + ''; // & see the magic happens
												}
											}
										}
									}]
								},
								tooltips: {
									callbacks: {
										label: function (item, data) {
											var label = data.datasets[item.datasetIndex].label || '';
											var yLabel = item.yLabel;
											var content = '';

											if (data.datasets.length > 1) {
												content += '<span class="popover-body-label mr-auto">' + label + '</span>';
											}

											content += '<span class="popover-body-value">' + yLabel + ' Modul</span>'; //pop-up details on chart
											return content;
										}
									}
								}
							},
							data: {
								labels: labels,
								datasets: [{
									// label: ['Acc. Done', ' Not Yet Started', ' On Progress'],
                  backgroundColor: ["#2dce89", "#f5223e", "#fb6340"], //warna tiap bar chart
									data: jml
								}]
							}
						});

						// Save to jQuery object

						$chart.data('chart', salesChart);

					};
					if ($chart.length) {
						init($chart);
					}

				})();
			}
		});
	}); 
  </script>
</body>

</html>