<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view("admin/_partials/head.php") ?> 
</head>

<body id="page-top">
    <?php $this->load->view("admin/_partials/navbar.php") ?>
    <div id="wrapper">

        <?php $this ->load->view("admin/_partials/sidebar.php") ?>
        
        <div id="content-wrapper">
            <div class="container-fluid">
                <?php $this->load->view("admin/_partials/breadcrumb.php") ?>

                 <!-- database -->

                 <div class="card mb-3">
                    <div class="card-header">
                        <a href="<?php echo site_url('admin/workbook/add') ?>"><i class="fas fa-plus"></i> Tambah Baru</a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>ID Stage</th>
                                        <th>ID Modul</th>
                                        <th>Nomor Pekerja</th>
                                        <th>Nama Pekerja</th>
                                        <th>Status</th>
                                        <th>Lembar Pengesahan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($workbook as $workbook): ?>
                                    <tr>
                                        <td width="150">
                                            <?php echo $workbook->stage_id ?>
                                        </td>
                                        <td>
                                            <?php echo $workbook->modul_id ?>
                                        </td>
                                        <td>
                                            <img src="<?php echo  base_url('upload/lembarsah/'.$workbook->image) ?>" width="64" />
                                        </td>
                                        <td class="small">
                                            <?php echo substr($workbook->keteranganp, 120) ?>...</td>
                                        <td width="250">
                                            <a href="<?php echo site_url('admin/workbook/edit'.$workbook->workbook_id)?>"
                                            class="btn btn-small"><i class="fas fa-edit"></i>Edit</a>
                                        <a onclick="deleteConfirm('<?php echo site_url('admin/workbook/delete'.$workbook->workbook_id) ?>')"
                                        href="#" class="btn btn-small text-danger"><i class="fas fa-trash"></i>Hapus</a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                 </div>

            </div>
            <!-- container fluid -->

            <!-- sticky footer -->
            <?php $this->load->view("admin/_partials/footer.php") ?>
        </div>
        <!-- wrapper -->

        <?php $this->load->view("admin/_partials/scrolltop.php") ?>
        <?php $this->load->view("admin/_partials/modal.php") ?>
        <?php $this->load->view("admin/_partials/js.php") ?>
    </div>
</body>
</html>