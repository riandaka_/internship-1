<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');

class Raport extends CI_Controller
{   
    public function __construct()
    {
        parent::__construct();
        $this->load->model("raport_model_user"); //load model workbook
        $this->load->library('form_validation'); //load library form validation
        $this->load->helper(array('form', 'url'));
        if($this->raport_model_user->is_role() != "user"){
            redirect("index.php/login");
        }
    }

    public function index()
    {
        $data["raport"] = $this->raport_model_user->getAll(); //ambil data dari model
        // print_r($data["raport"]); die;
        $this->load->view("user/raport/list_raport", $data); //load view data model ke workbook
    }
}