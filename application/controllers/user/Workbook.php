<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');

class Workbook extends CI_Controller
{   
    public function __construct()
    {
        parent::__construct();
        $this->load->model("workbook_model_user"); //load model workbook
        $this->load->library('form_validation'); //load library form validation
        $this->load->helper(array('form', 'url'));
        if($this->workbook_model_user->is_role() != "user"){
            redirect("index.php/login");
        }
    }

    public function index()
    {
        $data["workbook"] = $this->workbook_model_user->getAll(); //ambil data dari model
        $this->load->view("user/workbook/list_workbook", $data); //load view data model ke workbook
    }

    public function get_stage()
    {
        $data['data'] = $this->workbook_model_user->get_data_stage();
        $this->load->view('user/workbook/new_form', $data);
    }

    public function get_modul()
    {
        $id = $this->input->post('id');
        $data = $this->workbook_model_user->get_data_modul($id);
        echo json_encode($data);
    }

    public function add()
    {
        // $workbook = $this->workbook_model; //objek model
        // $validation = $this->form_validation; //objek form validation
        // $validation->set_rules($workbook->rules()); //terapkan rules
        
        // if ($validation->run()){
        //     $workbook->save();
        
        // }else{
        //     print_r("tes gagal"); die;
        // }
        

        $data["workbook"] = $this->workbook_model_user->get_data_stage();
        $this->load->view("user/workbook/new_form", $data); //load isi form workbook
    }

    public function aksi_add()
    {
    //    print_r('tes'); die; 
        $this->session->set_flashdata('Sukses', 'Data Anda Berhasil Disimpan'); //pesan berhasil
        $this->workbook_model_user->save(); //objek modelredir
        redirect('index.php/user/workbook');
    }

    public function edit($id=null)
    {
        // if(!isset($id)) redirect('user/workbook/list_workbook');

        // $workbook = $this->workbook_model;
        // $validation = $this->form_validation;
        // $validation->set_rules($workbook->rules());

        // if ($validation->run()){
        //     $workbook->update();
        // }else{
        //     print_r("tes gagal"); die;
        
        
        $data["workbook"] = $this->workbook_model_user->getById($id); //mengambil data berdasarkan id
        if (!$data["workbook"]) show_404();// jika tidak ada show error
        $this->load->view("user/workbook/edit_form", $data); //load edit form workbook
    }

    public function aksi_edit()
    {
        $config['upload_path'] = './upload/workbook'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan


         $this->load->library('upload', $config);
         if ( ! $this->upload->do_upload('image')){
        $id = $this->input->post('workbook_id');
                    $data = array (
                        'workbook_id' => $this->input->post('workbook_id'),
                        'stage_id' => $this->input->post('stage_id'),
                        'modul_id' => $this->input->post('modul_id'),
                        'progress' => $this->input->post('progress'),
                        'status' => $this->input->post('status'),
                        'image' => $this->input->post('old_image')
                    );

                        // print_r($data); die;
                    $this->workbook_model_user->update($id,$data);
                    $this->session->set_flashdata('success', 'Data Anda Berhasil Diupdate');
                    redirect('index.php/user/workbook');
         }else{
        $id = $this->input->post('workbook_id');
            $image = $this->upload->data();
                    $data = array (

                        'workbook_id' => $this->input->post('workbook_id'),
                        'stage_id' => $this->input->post('stage_id'),
                        'modul_id' => $this->input->post('modul_id'),
                        'progress' => $this->input->post('progress'),
                        'status' => $this->input->post('status'),
                        'image' => $image['file_name']
                    );
                        // print_r($data); die;
                    $this->workbook_model_user->update($id,$data);
                    $this->session->set_flashdata('success', 'Data Anda Berhasil Diupdate');
                    redirect('index.php/user/workbook');
                }
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->workbook_model_user->delete($id)){
            redirect('index.php/user/workbook');
        }
    }

    // public function aksi_upload()
    // {
    //     $config['upload_path']          = './upload/workbook/';
	// 	$config['allowed_types']        = 'gif|jpg|png';
	// 	$config['max_size']             = 10240;
	// 	// $config['max_width']            = 1024;
    //     // $config['max_height']           = 768;
        
    //     if ( ! $this->upload->do_upload('image')){
	// 		$error = array('error' => $this->upload->display_errors());
	// 		$this->load->view('admin/workbook/edit_form', $error);
	// 	}else{
	// 		$data = array('upload_data' => $this->upload->data());
	// 		$this->load->view('admin/workbook', $data);
    //     }
    // }
}
