<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');

class Account extends CI_Controller
{   
    public function __construct()
    {
        parent::__construct();
        $this->load->model("account_model"); //load model Account
        $this->load->library('form_validation'); //load library form validation
        if($this->account_model->is_role() != "admin"){
            redirect("index.php/login");
        }
    }

    public function index()
    {
        $data["account"] = $this->account_model->getAll(); //ambil data dari model
        $this->load->view("admin/account/list_account", $data); //load view data model ke account
    }

    public function add()
    {
        $account = $this->account_model; //objek model
        $validation = $this->form_validation; //objek form validation
        $validation->set_rules($account->rules()); //terapkan rules

        if ($validation->run()){
            $account->save();
            $this->session->set_flashdata('Sukses', 'Data Anda Berhasil Disimpan'); //pesan berhasil
        }
        
        $this->load->view("admin/account/new_account"); //load isi form account
    }

    public function edit($id=null)
    {
        if(!isset($id)) redirect('admin/account/list_account');

        $account = $this->account_model;
        $validation = $this->form_validation;
        $validation->set_rules($account->rules());

        if ($validation->run()){
            $account->update();
            $this->session->set_flashdata('success', 'Data Anda Berhasil Diupdate'); //pesan berhasil
        }

        $data["account"] = $account->getById($id); //mengambil data berdasarkan id
        if (!$data["account"]) show_404();// jika tidak ada show error
        $this->load->view("admin/account/edit_account", $data); //load edit form account
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->account_model->delete($id)){
            redirect('index.php/admin/account');
        }
    }
}
