<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');

class Assessment extends CI_Controller
{   
    public function __construct()
    {
        parent::__construct();
        $this->load->model("assessment_model"); //load model workbook
        $this->load->library('form_validation'); //load library form validation
        $this->load->helper(array('form', 'url'));
        if($this->assessment_model->is_role() != "admin"){
            redirect("index.php/login");
        }
    }

    public function index()
    {
        $data["assessment"] = $this->assessment_model->getAll(); //ambil data dari model
        // print_r('tes'); die;
        $this->load->view("admin/assessment/list_assessment", $data); //load view data model ke workbook
    }

    public function get_stage()
    {
        $data['data'] = $this->assessment_model->get_data_stage();
        $this->load->view('admin/assessment/new_assessment', $data);
    }

    public function get_modul()
    {
        $id = $this->input->post('id');
        $data = $this->assessment_model->get_data_modul($id);
        echo json_encode($data);
    }

    public function add()
    {
        $data["assessment"] = $this->assessment_model->get_data_stage();
        $this->load->view("admin/assessment/new_assessment", $data); //load isi form workbook
    }

    public function aksi_add()
    {
    //    print_r('tes'); die; 
        $this->session->set_flashdata('Sukses', 'Data Anda Berhasil Disimpan'); //pesan berhasil
        $this->assessment_model->save(); //objek modelredir
        redirect('index.php/admin/assessment');
    }

    public function edit($id=null)
    {   
        $data["assessment"] = $this->assessment_model->getById($id); //mengambil data berdasarkan id
        if (!$data["assessment"]) show_404();// jika tidak ada show error
        $this->load->view("admin/assessment/edit_assessment", $data); //load edit form workbook
    }

    public function aksi_edit()
    {
                $id = $this->input->post('assessment_id');

                $nilai_definisi = $this->input->post('nilai_definisi');
                $nilai_definisi *= 0.1;
                $nilai_tujuan = $this->input->post('nilai_tujuan');
                $nilai_tujuan *= 0.1;
                $nilai_langkah = $this->input->post('nilai_langkah');
                $nilai_langkah *= 0.2;
                $nilai_contoh = $this->input->post('nilai_contoh');
                $nilai_contoh *= 0.15;
                $nilai_analisis = $this->input->post('nilai_analisis');
                $nilai_analisis *= 0.2;
                $nilai_solusi = $this->input->post('nilai_solusi');
                $nilai_solusi *= 0.25;
                $total_nilai = $nilai_definisi + $nilai_tujuan + $nilai_langkah + $nilai_contoh + $nilai_analisis + $nilai_solusi;
                $status_lulus = $nilai_definisi + $nilai_tujuan + $nilai_langkah + $nilai_contoh + $nilai_analisis + $nilai_solusi;

                if($total_nilai >= 70){
                    $status_lulus = "Lulus";
                }else{
                    $status_lulus = "Tidak Lulus";
                }
                    $data = array (
                        'assessment_id' => $this->input->post('assessment_id'),
                        'stage_id' => $this->input->post('stage_id'),
                        'modul_id' => $this->input->post('modul_id'),
                        'employee_id' => $this->input->post('employee_id'),
                        'nilai_definisi' => $this->input->post('nilai_definisi'),
                        'nilai_tujuan' => $this->input->post('nilai_tujuan'),
                        'nilai_langkah' => $this->input->post('nilai_langkah'),
                        'nilai_contoh' => $this->input->post('nilai_contoh'),
                        'nilai_analisis' => $this->input->post('nilai_analisis'),
                        'nilai_solusi' => $this->input->post('nilai_solusi'),
                        'total_nilai' => $total_nilai,
                        'status_lulus' => $status_lulus
                    );

                        // print_r($data); die;
                    $this->assessment_model->update($id,$data);
                    $this->session->set_flashdata('success', 'Data Anda Berhasil Diupdate');
                    redirect('index.php/admin/assessment');
        }
    

    public function delete($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->assessment_model->delete($id)){
            redirect('index.php/admin/assessment');
        }
    }
}
