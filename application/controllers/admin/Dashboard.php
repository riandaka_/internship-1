<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    public function __construct()
    {
        parent:: __construct();
        //load model admin
        $this->load->model('admin');
        //cek session dan level user
        if($this->admin->is_role() != "admin"){
            redirect("index.php/login");
        }
    }   

    public function index()
    {
        //load view admin/dashboard.php
        $data['jumlah_peserta'] = $this->admin->jumlah_peserta();
        $data['jumlah_modul'] = $this->admin->jumlah_modul();
        // print_r($data);die;
        $this->load->view("admin/dashboard",$data);
    }
} 