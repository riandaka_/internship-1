<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');

class Ruangkerja extends CI_Controller
{   
    private $filename = "import_data"; // Kita tentukan nama filenya
    public function __construct()
    {
        parent::__construct();
        $this->load->model("ruangkerja_model"); //load model workbook
        $this->load->library('form_validation'); //load library form validation
        $this->load->helper('form');
        $this->load->helper('url');
        if($this->ruangkerja_model->is_role() != "admin"){
            redirect("index.php/login");
        }
    }

    public function index()
    {
        $data["ruangkerja"] = $this->ruangkerja_model->getAll(); //ambil data dari model
        // print_r('tes'); die;
        $this->load->view("admin/ruangkerja/list_ruangkerja", $data); //load view data model ke workbook
    }


    public function add()
    {
        $data["ruangkerja"] = $this->ruangkerja_model->get_data_stage();
        $this->load->view("admin/ruangkerja/new_ruangkerja", $data); //load isi form workbook
    }

    public function aksi_add()
    {
    //    print_r('tes'); die; 
        $this->session->set_flashdata('Sukses', 'Data Anda Berhasil Disimpan'); //pesan berhasil
        $this->ruangkerja_model->save(); //objek modelredir
        redirect('index.php/admin/ruangkerja');
    }

    public function edit($id=null)
    {   
        $data["ruangkerja"] = $this->ruangkerja_model->getById($id); //mengambil data berdasarkan id
        if (!$data["ruangkerja"]) show_404();// jika tidak ada show error
        $this->load->view("admin/ruangkerja/edit_ruangkerja", $data); //load edit form workbook
    }

    public function aksi_edit()
    {
                $id = $this->input->post('ruangkerja_id');

                $data = array (
                    "ruangkerja_id" => $this->input->post("ruangkerja_id"),
                    "employee_id" => $this->input->post("employee_id"),
                    "employee_name" => $this->input->post("employee_name"),
                    "user_email" => $this->input->post("user_email"),
                    "start_time" => $this->input->post("start_time"),
                    "posttest_score" => $this->input->post("posttest_score"),
                    "status" => $this->input->post("status"),
                    "deadline_course" => $this->input->post("deadline_course"),
                    "clear_time" => $this->input->post("clear_time"),
                    "directorate" => $this->input->post("directorate")
                    );

                        // print_r($data); die;
                    $this->ruangkerja_model->update($id,$data);
                    $this->session->set_flashdata('success', 'Data Anda Berhasil Diupdate');
                    redirect('index.php/admin/ruangkerja');
        }
    

    public function delete($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->ruangkerja->delete($id)){
            redirect('index.php/admin/ruangkerja');
        }
    }

    public function formImport(){
        $data = array(); // Buat variabel $data sebagai array
        
        if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
            // lakukan upload file dengan memanggil function upload yang ada di ModelAdmin.php
            $upload = $this->ruangkerja_model->upload_file($this->filename);
            
            if($upload['result'] == "success"){ // Jika proses upload sukses
                // Load plugin PHPExcel nya
                include APPPATH.'third_party/PHPExcel/PHPExcel.php';
                
                $excelreader = new PHPExcel_Reader_Excel2007();
                $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
                $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
                
                // Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
                // Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
                $data['sheet'] = $sheet; 
            }else{ // Jika proses upload gagal
                $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
            }
        }
        $this->load->view('admin/ruangkerja/import_ruangkerja', $data);
    }
    
    public function import(){
        $this->db->empty_table('ruangkerja');
        $this->ruangkerja_model->autoIncrementRuangkerja();

        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';
        
        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
        
        // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
        $data = array();
        
        $numrow = 1;
        foreach($sheet as $row){
            // Cek $numrow apakah lebih dari 1
            // Artinya karena baris pertama adalah nama-nama kolom
            // Jadi dilewat saja, tidak usah diimport
            if($numrow > 1){
                // Kita push (add) array data ke variabel data
                 array_push($data, array(
          'employee_id'=>$row['A'], // Insert data nis dari kolom A di excel
          'employee_name'=>$row['B'], // Insert data nis dari kolom A di excel
          'user_email'=>$row['C'], // Insert data nama dari kolom B di excel
          'start_time'=>$row['D'], // Insert data jenis kelamin dari kolom C di excel
          'posttest_score'=>$row['E'], // Insert data alamat dari kolom D di excel
          'status'=>$row['F'], // Insert data alamat dari kolom D di excel
          'deadline_course'=>$row['G'], // Insert data alamat dari kolom D di excel
          'clear_time'=>$row['H'], // Insert data alamat dari kolom D di excel
          'directorate'=>$row['I'], // Insert data alamat dari kolom D di excel
        ));
            }
            
            $numrow++; // Tambah 1 setiap kali looping
        }

        // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
        $this->ruangkerja_model->importExcel($data);
        
        redirect("index.php/admin/ruangkerja"); // Redirect ke halaman awal (ke controller admin fungsi index)
    }
}
