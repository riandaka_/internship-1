<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');

class Raport extends CI_Controller
{   
    public function __construct()
    {
        parent::__construct();
        $this->load->model("raport_model"); //load model workbook
        $this->load->library('form_validation'); //load library form validation
        $this->load->helper(array('form', 'url'));
        if($this->raport_model->is_role() != "admin"){
            redirect("index.php/login");
        }
    }

    public function index()
    {
        $data["raport"] = $this->raport_model->getAll(); //ambil data dari model
        // print_r($data["raport"]); die;
        $this->load->view("admin/raport/list_raport", $data); //load view data model ke workbook
    }
}