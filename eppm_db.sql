-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2020 at 03:10 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eppm_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `employee_id` int(6) NOT NULL,
  `directorate` varchar(50) NOT NULL,
  `employee_name` varchar(50) NOT NULL,
  `username` varchar(12) NOT NULL,
  `password` varchar(12) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `role` enum('admin','user') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`employee_id`, `directorate`, `employee_name`, `username`, `password`, `user_email`, `role`) VALUES
(1, 'admin', 'admin', 'admin', 'admin123', 'admin@admin.com', 'admin'),
(41, 'werwer', 'wtwr', 'ww', 'rwe', 'werw', 'admin'),
(123456, 'Pengolahan', 'Riandaka Rizal', 'riandakar20', 'riandaka123', 'riandaka20@gmail.com', 'user'),
(705423, 'Megaproyek Pengolahan dan Petrokimia', 'Bambang Purwito H.', 'bambangpurwi', 'bambangpurwi', 'bampur@pertamina.com', 'user'),
(706169, 'Megaproyek Pengolahan dan Petrokimia', 'Widodo Tri Rahardjo', 'widodotri', 'widodotri123', 'widodo.rahardjo@pertamina.com', 'user'),
(706655, 'Megaproyek Pengolahan dan Petrokimia', 'Arie Wisianto', 'ariewisianto', 'ariewisianto', 'arie.wisianto@pertamina.com', 'user'),
(708097, 'Megaproyek Pengolahan dan Petrokimia', 'Syamsul Bahri', 'syamsulbahri', 'syamsulbahri', 'sbahri@pertamina.com', 'user'),
(708104, 'Megaproyek Pengolahan dan Petrokimia', 'I Gusti Agung Nyoman Bismayudha', 'igustiagung', 'igustiagung1', 'igan.bismayudha@pertamina.com', 'user'),
(708201, 'Megaproyek Pengolahan dan Petrokimia', 'Imam Sunarto', 'imamsunarto', 'imamsunarto1', 'i_sunarto@pertamina.com', 'user'),
(708218, 'Megaproyek Pengolahan dan Petrokimia', 'Dwi Wibisana', 'dwiwibisana', 'dwiwibisana1', 'dwibisana@pertamina.com', 'user'),
(708372, 'Megaproyek Pengolahan dan Petrokimia', 'Rahadi Wicaksono', 'rahadiwicaks', 'rahadiwicaks', 'rahadiw@pertamina.com', 'user'),
(708397, 'Megaproyek Pengolahan dan Petrokimia', 'Sahadi', 'sahadr', 'sahadr123', 'sahadi@pertamina.com', 'user'),
(708753, 'Megaproyek Pengolahan dan Petrokimia', 'Eiman', 'eiman', 'eiman123', 'eiman@pertamina.com', 'user'),
(708786, 'Megaproyek Pengolahan dan Petrokimia', 'Ari Dwikoranto', 'aridwikorant', 'aridwikorant', 'ari.dwikoranto@pertamina.com', 'user'),
(709044, 'Megaproyek Pengolahan dan Petrokimia', 'Suwahyanto', 'suwahranto', 'suwahranto12', 'suwahyanto@pertamina.com', 'user'),
(709214, 'Megaproyek Pengolahan dan Petrokimia', 'Jadi Purwoko', 'jadipurwoko', 'jadipurwoko1', 'jadi@pertamina.com', 'user'),
(709377, 'Megaproyek Pengolahan dan Petrokimia', 'Dwina Candraasih', 'dwinacandraa', 'dwinacandraa', 'dwina@pertamina.com', 'user'),
(713604, 'Megaproyek Pengolahan dan Petrokimia', 'Priyo Titi Budhi', 'priyotiti', 'priyotiti123', 'priyotb@pertamina.com', 'user'),
(713726, 'Megaproyek Pengolahan dan Petrokimia', 'M.Daud', 'mdaud', 'mdaud123', 'mdaud@pertamina.com', 'user'),
(713848, 'Megaproyek Pengolahan dan Petrokimia', 'Mochamad Heru Andika', 'mochamadheru', 'mochamadheru', 'mochamad.andika@pertamina.com', 'user'),
(713912, 'Megaproyek Pengolahan dan Petrokimia', 'Khairul Amal', 'khairulamal', 'khairulamal1', 'k.amal@pertamina.com', 'user'),
(714163, 'Megaproyek Pengolahan dan Petrokimia', 'Ali Dwiyono Haritsyah', 'alidwiyono', 'alidwiyono12', 'ad_haritsyah@pertamina.com', 'user'),
(714196, 'Megaproyek Pengolahan dan Petrokimia', 'Soegih Widhodho', 'soegihwidhod', 'soegihwidhod', 'soegih@pertamina.com', 'user'),
(714228, 'Megaproyek Pengolahan dan Petrokimia', 'Waluyo Subiyanto', 'waluyosubiya', 'waluyosubiya', 'waluyo_s9871@pertamina.com', 'user'),
(714252, 'Megaproyek Pengolahan dan Petrokimia', 'Usman Hasan', 'usmanhasan', 'usmanhasan12', 'usmanh@pertamina.com', 'user'),
(714341, 'Megaproyek Pengolahan dan Petrokimia', 'Yuherwin', 'yuherrin', 'yuherrin123', 'yuherwin@pertamina.com', 'user'),
(714569, 'Megaproyek Pengolahan dan Petrokimia', 'Hery Yudhianto', 'heryyudhiant', 'heryyudhiant', 'hery_y@pertamina.com', 'user'),
(714585, 'Megaproyek Pengolahan dan Petrokimia', 'Rully Hendarsetiawan', 'rullyhendars', 'rullyhendars', 'rully_h@pertamina.com', 'user'),
(714593, 'Megaproyek Pengolahan dan Petrokimia', 'Arif Budiyono', 'arifbudiyono', 'arifbudiyono', 'arif@pertamina.com', 'user'),
(714666, 'Megaproyek Pengolahan dan Petrokimia', 'Syarif Hidayat', 'syarifhidaya', 'syarifhidaya', 'syh4666@pertamina.com', 'user'),
(714682, 'Megaproyek Pengolahan dan Petrokimia', 'Handi Soejanto Ateng', 'handisoejant', 'handisoejant', 'handi@pertamina.com', 'user'),
(714739, 'Megaproyek Pengolahan dan Petrokimia', 'Efrizon', 'efrizon', 'efrizon123', 'efrizon@pertamina.com', 'user'),
(714796, 'Megaproyek Pengolahan dan Petrokimia', 'Judy Pudji Tresnacahyono', 'judypudji', 'judypudji123', 'judy@pertamina.com', 'user'),
(717063, 'Megaproyek Pengolahan dan Petrokimia', 'Yoyok Poerwedi', 'yoyokpoerwed', 'yoyokpoerwed', 'ypoerwedi@pertamina.com', 'user'),
(717071, 'Megaproyek Pengolahan dan Petrokimia', 'Kadek Ambara Jaya', 'kadekambara', 'kadekambara1', 'kadek@pertamina.com', 'user'),
(717103, 'Megaproyek Pengolahan dan Petrokimia', 'Sigit Pradjaka S.', 'sigitpradjak', 'sigitpradjak', 'sigit_ps@pertamina.com', 'user'),
(717225, 'Megaproyek Pengolahan dan Petrokimia', 'Budi Anggoro Priyo', 'budianggoro', 'budianggoro1', 'budi.priyo@pertamina.com', 'user'),
(717233, 'Megaproyek Pengolahan dan Petrokimia', 'Kustinah', 'kustinah', 'kustinah123', 'kustinah@pertamina.com', 'user'),
(717322, 'Megaproyek Pengolahan dan Petrokimia', 'Herry Yawarto', 'herryyawarto', 'herryyawarto', 'yawarto@pertamina.com', 'user'),
(717541, 'Megaproyek Pengolahan dan Petrokimia', 'Antonius Sianipar', 'antoniussian', 'antoniussian', 'antonius.sianipar@pertamina.com', 'user'),
(717688, 'Megaproyek Pengolahan dan Petrokimia', 'Harmen', 'harmen', 'harmen123', 'harmenup2@pertamina.com', 'user'),
(717777, 'Megaproyek Pengolahan dan Petrokimia', 'Syaifuddin Azhar', 'syaifuddinaz', 'syaifuddinaz', 's_azhar@pertamina.com', 'user'),
(717874, 'Megaproyek Pengolahan dan Petrokimia', 'Pranoto Hutomo', 'pranotohutom', 'pranotohutom', 'pranoto@pertamina.com', 'user'),
(718027, 'Megaproyek Pengolahan dan Petrokimia', 'Abdullah', 'abdullah', 'abdullah123', 'abdullah2708@pertamina.com', 'user'),
(718149, 'Megaproyek Pengolahan dan Petrokimia', 'Wiko Taviarto', 'wikotaviarto', 'wikotaviarto', 'wiko@pertamina.com', 'user'),
(718205, 'Megaproyek Pengolahan dan Petrokimia', 'Djoko Koen Soewito', 'djokokoen', 'djokokoen123', 'djokokoen@pertamina.com', 'user'),
(718335, 'Megaproyek Pengolahan dan Petrokimia', 'Hidayaturrahim', 'hidayaturrah', 'hidayaturrah', 'hidayaturrahim@pertamina.com', 'user'),
(718343, 'Megaproyek Pengolahan dan Petrokimia', 'Supriyono', 'suprirono', 'suprirono123', 'supriyono-ts@pertamina.com', 'user'),
(718368, 'Megaproyek Pengolahan dan Petrokimia', 'Yosep Asro Wain', 'yosepasro', 'yosepasro123', 'yosep.wain@pertamina.com', 'user'),
(718498, 'Megaproyek Pengolahan dan Petrokimia', 'Manson Sihotang', 'mansonsihota', 'mansonsihota', 'manson.sihotang@pertamina.com', 'user'),
(718554, 'Megaproyek Pengolahan dan Petrokimia', 'Aris Suparto', 'arissuparto', 'arissuparto1', 'ariss@pertamina.com', 'user'),
(719453, 'Megaproyek Pengolahan dan Petrokimia', 'Asril Bijaksana', 'asrilbijaksa', 'asrilbijaksa', 'asril_b@pertamina.com', 'user'),
(719923, 'Megaproyek Pengolahan dan Petrokimia', 'M. Cholik Fauzi', 'mcholik', 'mcholik123', 'cholik.fauzi@pertamina.com', 'user'),
(719972, 'Megaproyek Pengolahan dan Petrokimia', 'Roberman Siburian', 'robermansibu', 'robermansibu', 'roberman_s@pertamina.com', 'user'),
(720124, 'Megaproyek Pengolahan dan Petrokimia', 'Hendra Onang Wijaya', 'hendraonang', 'hendraonang1', 'how@pertamina.com', 'user'),
(720157, 'Megaproyek Pengolahan dan Petrokimia', 'Suharta Heru Pujiono', 'suhartaheru', 'suhartaheru1', 'suharta@pertamina.com', 'user'),
(724231, 'Megaproyek Pengolahan dan Petrokimia', 'Deded Hermawan', 'dededhermawa', 'dededhermawa', 'dededhermawan@pertamina.com', 'user'),
(726451, 'Megaproyek Pengolahan dan Petrokimia', 'Kahfi', 'kahfirfi', 'kahfirfi123', 'kahfi@pertamina.com', 'user'),
(730339, 'Megaproyek Pengolahan dan Petrokimia', 'Gesit Ngudi Raharjo', 'gesitngudi', 'gesitngudi12', 'grahardjo@pertamina.com', 'user'),
(730696, 'Megaproyek Pengolahan dan Petrokimia', 'Rini Fenti Astuti', 'rinifenti', 'rinifenti123', 'fenti@pertamina.com', 'user'),
(731108, 'Megaproyek Pengolahan dan Petrokimia', 'Rudy Samsulhadi', 'rudysamsulha', 'rudysamsulha', 'rudys@pertamina.com', 'user'),
(734195, 'Megaproyek Pengolahan dan Petrokimia', 'Sutopo', 'sutopr', 'sutopr123', 'stopo@pertamina.com', 'user'),
(734235, 'Megaproyek Pengolahan dan Petrokimia', 'James Hengki Mantak', 'jameshengki', 'jameshengki1', 'jims@pertamina.com', 'user'),
(734851, 'Megaproyek Pengolahan dan Petrokimia', 'Achmad Muslimin', 'achmadmuslim', 'achmadmuslim', 'muslimin@pertamina.com', 'user'),
(735004, 'Megaproyek Pengolahan dan Petrokimia', 'H.Kemas A.Johansyah', 'hkemas', 'hkemas123', 'kemas-aj@pertamina.com', 'user'),
(736293, 'Megaproyek Pengolahan dan Petrokimia', 'Tedjo Barata Rm', 'tedjobarata', 'tedjobarata1', 'tedjob@pertamina.com', 'user'),
(736625, 'Megaproyek Pengolahan dan Petrokimia', 'Firman Hakim', 'firmanhakim', 'firmanhakim1', 'fiha@pertamina.com', 'user'),
(738083, 'Megaproyek Pengolahan dan Petrokimia', 'Marthen Linggi Pasorong', 'marthenlingg', 'marthenlingg', 'marthen.pasorong@pertamina.com', 'user'),
(739088, 'Megaproyek Pengolahan dan Petrokimia', 'Rekson Junior Rumahorbo', 'reksonjunior', 'reksonjunior', 'rekson@pertamina.com', 'user'),
(739906, 'Megaproyek Pengolahan dan Petrokimia', 'Bambang Harimurti', 'bambangharim', 'bambangharim', 'bambangh@pertamina.com', 'user'),
(739971, 'Megaproyek Pengolahan dan Petrokimia', 'Rohmadi', 'rohmari', 'rohmari123', 'rohmadi@pertamina.com', 'user'),
(739996, 'Megaproyek Pengolahan dan Petrokimia', 'Sumarno', 'sumarro', 'sumarro123', 'sumarno73@pertamina.com', 'user'),
(740001, 'Megaproyek Pengolahan dan Petrokimia', 'Agung Budi Setiono', 'agungbudi', 'agungbudi123', 'agungb@pertamina.com', 'user'),
(740034, 'Megaproyek Pengolahan dan Petrokimia', 'I Gusti Bagus Prihanta', 'igustibagus', 'igustibagus1', 'gbprihanta@pertamina.com', 'user'),
(740067, 'Megaproyek Pengolahan dan Petrokimia', 'Albin Ginting', 'albinginting', 'albinginting', 'albin@pertamina.com', 'user'),
(740091, 'Megaproyek Pengolahan dan Petrokimia', 'Erwin Thamrin', 'erwinthamrin', 'erwinthamrin', 'erwin.thamrin@pertamina.com', 'user'),
(741071, 'Megaproyek Pengolahan dan Petrokimia', 'Agus Suryono', 'agussuryono', 'agussuryono1', 'agus.suryono@pertamina.com', 'user'),
(741111, 'Megaproyek Pengolahan dan Petrokimia', 'Arie Gumilar', 'ariegumilar', 'ariegumilar1', 'ariegumilar@pertamina.com', 'user'),
(741152, 'Megaproyek Pengolahan dan Petrokimia', 'Dony Satrio Adi', 'donysatrio', 'donysatrio12', 'dony@pertamina.com', 'user'),
(741185, 'Megaproyek Pengolahan dan Petrokimia', 'Hendro Susanto', 'hendrosusant', 'hendrosusant', 'hendro.susanto@pertamina.com', 'user'),
(741233, 'Megaproyek Pengolahan dan Petrokimia', 'Iwan Priyono', 'iwanpriyono', 'iwanpriyono1', 'iwanpri@pertamina.com', 'user'),
(746570, 'Megaproyek Pengolahan dan Petrokimia', 'Hendrikawan Mulyadi', 'hendrikawanm', 'hendrikawanm', 'hendrikawan.mulyadi@pertamina.com', 'user'),
(746574, 'Megaproyek Pengolahan dan Petrokimia', 'Imron', 'imronron', 'imronron123', 'imron_pe@pertamina.com', 'user'),
(746583, 'Megaproyek Pengolahan dan Petrokimia', 'Rusmayadi Wardhana', 'rusmayadiwar', 'rusmayadiwar', 'rusmayadi@pertamina.com', 'user'),
(746924, 'Megaproyek Pengolahan dan Petrokimia', 'Hariadi', 'hariadi', 'hariadi123', 'hariadi@pertamina.com', 'user'),
(746927, 'Megaproyek Pengolahan dan Petrokimia', 'Iman Sukmajati', 'imansukmajat', 'imansukmajat', 'iman_s@pertamina.com', 'user'),
(747009, 'Megaproyek Pengolahan dan Petrokimia', 'ria handayani', 'riahandayani', 'riahandayani', 'ria.handayani@pertamina.com', 'user'),
(747533, 'Megaproyek Pengolahan dan Petrokimia', 'Irfon Wahana Putra', 'irfonwahana', 'irfonwahana1', 'irfon.putra@pertamina.com', 'user'),
(747547, 'Megaproyek Pengolahan dan Petrokimia', 'Sigit Heru Susanto', 'sigitheru', 'sigitheru123', 'sigit.susanto@pertamina.com', 'user'),
(747887, 'Megaproyek Pengolahan dan Petrokimia', 'Chitra Dewi', 'chitradewi', 'chitradewi12', 'chitra.dewi@pertamina.com', 'user'),
(747889, 'Megaproyek Pengolahan dan Petrokimia', 'Ery Gunarto', 'erygunarto', 'erygunarto12', 'ery.gunarto@pertamina.com', 'user'),
(747896, 'Megaproyek Pengolahan dan Petrokimia', 'Mu\'ti Fahmi', 'mutifahmi', 'mutifahmi123', 'mu\'ti.fahmi@pertamina.com', 'user'),
(747899, 'Megaproyek Pengolahan dan Petrokimia', 'Rendra Jayantara Putra', 'rendrajayant', 'rendrajayant', 'rendra.jayantara@pertamina.com', 'user'),
(747975, 'Megaproyek Pengolahan dan Petrokimia', 'John Muzibur Simamora', 'johnmuzibur', 'johnmuzibur1', 'john.simamora@pertamina.com', 'user'),
(748087, 'Megaproyek Pengolahan dan Petrokimia', 'Daniel Romulus', 'danielromulu', 'danielromulu', 'daniel.romulus@pertamina.com', 'user'),
(748513, 'Megaproyek Pengolahan dan Petrokimia', 'Hermanto', 'hermanto', 'hermanto123', 'hermanto.sec@pertamina.com', 'user'),
(748623, 'Megaproyek Pengolahan dan Petrokimia', 'Ahmad Aminulloh', 'ahmadaminull', 'ahmadaminull', 'ahmad.aminulloh@pertamina.com', 'user'),
(748628, 'Megaproyek Pengolahan dan Petrokimia', 'Andi Hidayat', 'andihidayat', 'andihidayat1', 'andi.hidayat@pertamina.com', 'user'),
(748630, 'Megaproyek Pengolahan dan Petrokimia', 'Anindita Mahendra', 'aninditamahe', 'aninditamahe', 'anindita.mahendra@pertamina.com', 'user'),
(748635, 'Megaproyek Pengolahan dan Petrokimia', 'Dadi Ahmad Mawardi', 'dadiahmad', 'dadiahmad123', 'dadi.mawardi@pertamina.com', 'user'),
(748639, 'Megaproyek Pengolahan dan Petrokimia', 'Erick Leonardo', 'erickleonard', 'erickleonard', 'erick.butar@pertamina.com', 'user'),
(748643, 'Megaproyek Pengolahan dan Petrokimia', 'Febriandi', 'febriandi', 'febriandi123', 'febriandi@pertamina.com', 'user'),
(748647, 'Megaproyek Pengolahan dan Petrokimia', 'Hengky', 'hengky', 'hengky123', 'hengky@pertamina.com', 'user'),
(748650, 'Megaproyek Pengolahan dan Petrokimia', 'Indra Ginanjar', 'indraginanja', 'indraginanja', 'indra.ginanjar@pertamina.com', 'user'),
(748654, 'Megaproyek Pengolahan dan Petrokimia', 'Muhammad Septiadi Anggoro', 'muhammadsept', 'muhammadsept', 'muhammad.septiadi@pertamina.com', 'user'),
(748660, 'Megaproyek Pengolahan dan Petrokimia', 'Ragil Priyanto', 'ragilpriyant', 'ragilpriyant', 'ragil.priyanto@pertamina.com', 'user'),
(748662, 'Megaproyek Pengolahan dan Petrokimia', 'Rano Karno Sitepu', 'ranokarno', 'ranokarno123', 'rano.sitepu@pertamina.com', 'user'),
(748666, 'Megaproyek Pengolahan dan Petrokimia', 'Sayidatul Khoiriyah', 'sayidatulkho', 'sayidatulkho', 'sayidatul.khoiriyah@pertamina.com', 'user'),
(748670, 'Megaproyek Pengolahan dan Petrokimia', 'Tofan Santoso', 'tofansantoso', 'tofansantoso', 'tofan.santoso@pertamina.com', 'user'),
(748671, 'Megaproyek Pengolahan dan Petrokimia', 'Umar Heru Setiadi', 'umarheru', 'umarheru123', 'umar.setiadi@pertamina.com', 'user'),
(748672, 'Megaproyek Pengolahan dan Petrokimia', 'Vika Prasetya Andika', 'vikaprasetya', 'vikaprasetya', 'vika.prasetya@pertamina.com', 'user'),
(748694, 'Megaproyek Pengolahan dan Petrokimia', 'Nur Rochman', 'nurrochman', 'nurrochman12', 'nur.rochman@pertamina.com', 'user'),
(748861, 'Megaproyek Pengolahan dan Petrokimia', 'Adithya Riswimbardi', 'adithyariswi', 'adithyariswi', 'adithya.riswimbardi@pertamina.com', 'user'),
(749811, 'Megaproyek Pengolahan dan Petrokimia', 'Ardian Eko Handoko', 'ardianeko', 'ardianeko123', 'ardian.eko@pertamina.com', 'user'),
(749814, 'Megaproyek Pengolahan dan Petrokimia', 'Chandra Ricardo Marulitua', 'chandraricar', 'chandraricar', 'chandra.simanjuntak@pertamina.com', 'user'),
(749830, 'Megaproyek Pengolahan dan Petrokimia', 'Mochamad Nurdiansyah', 'mochamadnurd', 'mochamadnurd', 'mochamad.nurdiansyah@pertamina.com', 'user'),
(749834, 'Megaproyek Pengolahan dan Petrokimia', 'Pancar Fransco', 'pancarfransc', 'pancarfransc', 'pancar.fransco@pertamina.com', 'user'),
(749845, 'Megaproyek Pengolahan dan Petrokimia', 'Yoppi Sutrisna', 'yoppisutrisn', 'yoppisutrisn', 'yoppi.sutrisna@pertamina.com', 'user'),
(750094, 'Megaproyek Pengolahan dan Petrokimia', 'Asep Cahyana', 'asepcahyana', 'asepcahyana1', 'asep.cahyana@pertamina.com', 'user'),
(750771, 'Megaproyek Pengolahan dan Petrokimia', 'Dini Septiadhanny', 'diniseptiadh', 'diniseptiadh', 'dhini.septiadhanny@pertamina.com', 'user'),
(750787, 'Megaproyek Pengolahan dan Petrokimia', 'Achmad Hattary', 'achmadhattar', 'achmadhattar', 'achmad.hattary@pertamina.com', 'user'),
(751674, 'Megaproyek Pengolahan dan Petrokimia', 'Ika Rosadevi', 'ikarosadevi', 'ikarosadevi1', 'ika.rosadevi@pertamina.com', 'user'),
(751864, 'Megaproyek Pengolahan dan Petrokimia', 'Srina Ansella', 'srinaansella', 'srinaansella', 'srina.ansella@pertamina.com', 'user'),
(751882, 'Megaproyek Pengolahan dan Petrokimia', 'Riani Rahma Vita Sari', 'rianirahma', 'rianirahma12', 'riani.vita.sari@pertamina.com', 'user'),
(753535, 'Megaproyek Pengolahan dan Petrokimia', 'Tri Basoeki Soelis Vichyanto', 'tribasoeki', 'tribasoeki12', 'tribasoeki@pertamina.com', 'user'),
(753623, 'Megaproyek Pengolahan dan Petrokimia', 'Agus Maktum', 'agusmaktum', 'agusmaktum12', 'agus.maktum@pertamina.com', 'user'),
(755286, 'Megaproyek Pengolahan dan Petrokimia', 'Yudha Prastyono', 'yudhaprastyo', 'yudhaprastyo', 'yudha.prastyono@pertamina.com', 'user'),
(755303, 'Megaproyek Pengolahan dan Petrokimia', 'Abdul Karim Amarullah', 'abdulkarim', 'abdulkarim12', 'abdul.amarullah@pertamina.com', 'user'),
(755304, 'Megaproyek Pengolahan dan Petrokimia', 'Achmad Syafi\'udin', 'achmadsyafiu', 'achmadsyafiu', 'achmad.syafiudin@pertamina.com', 'user'),
(755305, 'Megaproyek Pengolahan dan Petrokimia', 'Adi Kurnia Muktabar', 'adikurnia', 'adikurnia123', 'adi.muktabar@pertamina.com', 'user'),
(755306, 'Megaproyek Pengolahan dan Petrokimia', 'Adrian Satriaji Wiryawan', 'adriansatria', 'adriansatria', 'adrian.wiryawan@pertamina.com', 'user'),
(755307, 'Megaproyek Pengolahan dan Petrokimia', 'Ahmad Obrain Ghifari', 'ahmadobrain', 'ahmadobrain1', 'ahmad.ghifari@pertamina.com', 'user'),
(755308, 'Megaproyek Pengolahan dan Petrokimia', 'Alfi Al Fahreizy', 'alfial', 'alfial123', 'alfi.fahreizy@pertamina.com', 'user'),
(755310, 'Megaproyek Pengolahan dan Petrokimia', 'Alvin Murad Rachmadsyah', 'alvinmurad', 'alvinmurad12', 'alvin.rachmadsyah@pertamina.com', 'user'),
(755311, 'Megaproyek Pengolahan dan Petrokimia', 'Amalia Dyah Arlita', 'amaliadyah', 'amaliadyah12', 'amalia.arlita@pertamina.com', 'user'),
(755312, 'Megaproyek Pengolahan dan Petrokimia', 'Anastasia Ayu Pratiwi', 'anastasiaayu', 'anastasiaayu', 'anastasia.pratiwi@pertamina.com', 'user'),
(755313, 'Megaproyek Pengolahan dan Petrokimia', 'Andrean Diyandana Filemon', 'andreandiyan', 'andreandiyan', 'andrean.filemon@pertamina.com', 'user'),
(755314, 'Megaproyek Pengolahan dan Petrokimia', 'anzhari luthfi', 'anzhariluthf', 'anzhariluthf', 'anzhari.luthfi@pertamina.com', 'user'),
(755315, 'Megaproyek Pengolahan dan Petrokimia', 'Azhar Hudaibie Adhitama', 'azharhudaibi', 'azharhudaibi', 'azhar.adhitama@pertamina.com', 'user'),
(755316, 'Megaproyek Pengolahan dan Petrokimia', 'Azka Afuza', 'azkaafuza', 'azkaafuza123', 'azka.nugroho@pertamina.com', 'user'),
(755317, 'Megaproyek Pengolahan dan Petrokimia', 'Bagaskara Dwi Anugrah', 'bagaskaradwi', 'bagaskaradwi', 'bagaskara.anugrah@pertamina.com', 'user'),
(755318, 'Megaproyek Pengolahan dan Petrokimia', 'Bagus Satrio Utomo P', 'bagussatrio', 'bagussatrio1', 'bagus.p@pertamina.com', 'user'),
(755319, 'Megaproyek Pengolahan dan Petrokimia', 'Bernando', 'bernardo', 'bernardo123', 'bernando@pertamina.com', 'user'),
(755320, 'Megaproyek Pengolahan dan Petrokimia', 'Bill Qishty Aulia Ardhy Arsya', 'billqishty', 'billqishty12', 'bill.arsya@pertamina.com', 'user'),
(755321, 'Megaproyek Pengolahan dan Petrokimia', 'Bonifacius Raditya Yudha Atmaja', 'bonifaciusra', 'bonifaciusra', 'bonifacius.atmaja@pertamina.com', 'user'),
(755322, 'Megaproyek Pengolahan dan Petrokimia', 'Chaidir Agam', 'chaidiragam', 'chaidiragam1', 'chaidir.ubaidillah@pertamina.com', 'user'),
(755323, 'Megaproyek Pengolahan dan Petrokimia', 'Chandra Dewi Rosalina', 'chandradewi', 'chandradewi1', 'chandra.rosalina@pertamina.com', 'user'),
(755324, 'Megaproyek Pengolahan dan Petrokimia', 'Cyntia Rachman', 'cyntiarachma', 'cyntiarachma', 'cyntia.rachman@pertamina.com', 'user'),
(755325, 'Megaproyek Pengolahan dan Petrokimia', 'Dany Pristiyan', 'danypristiya', 'danypristiya', 'dany.pristiyan@pertamina.com', 'user'),
(755326, 'Megaproyek Pengolahan dan Petrokimia', 'Destyawan Saputra', 'destyawansap', 'destyawansap', 'destyawan.saputra@pertamina.com', 'user'),
(755327, 'Megaproyek Pengolahan dan Petrokimia', 'Dewanta Priatama', 'dewantapriat', 'dewantapriat', 'dewanta.priatama@pertamina.com', 'user'),
(755328, 'Megaproyek Pengolahan dan Petrokimia', 'Dian Anggraini', 'diananggrain', 'diananggrain', 'dian.anggraini@pertamina.com', 'user'),
(755330, 'Megaproyek Pengolahan dan Petrokimia', 'Dionisius Andy Kristanto', 'dionisiusand', 'dionisiusand', 'dionisius.kristanto@pertamina.com', 'user'),
(755331, 'Megaproyek Pengolahan dan Petrokimia', 'Dwi Budianto', 'dwibudianto', 'dwibudianto1', 'budianto.dwi@pertamina.com', 'user'),
(755332, 'Megaproyek Pengolahan dan Petrokimia', 'Elmidian Rizky', 'elmidianrizk', 'elmidianrizk', 'elmidian.rizky@pertamina.com', 'user'),
(755333, 'Megaproyek Pengolahan dan Petrokimia', 'Eviana Dewi Setiawati', 'evianadewi', 'evianadewi12', 'eviana.setiawati@pertamina.com', 'user'),
(755334, 'Megaproyek Pengolahan dan Petrokimia', 'F Alverina Zagita', 'falverina', 'falverina123', 'alverina.zagita@pertamina.com', 'user'),
(755336, 'Megaproyek Pengolahan dan Petrokimia', 'Farida Arisa', 'faridaarisa', 'faridaarisa1', 'farida.arisa@pertamina.com', 'user'),
(755337, 'Megaproyek Pengolahan dan Petrokimia', 'Felix Samuel', 'felixsamuel', 'felixsamuel1', 'felix.samuel@pertamina.com', 'user'),
(755338, 'Megaproyek Pengolahan dan Petrokimia', 'Gema A Firmansyah', 'gemaa', 'gemaa123', 'gema.firmansyah@pertamina.com', 'user'),
(755339, 'Megaproyek Pengolahan dan Petrokimia', 'Ghassani Feta Adani', 'ghassanifeta', 'ghassanifeta', 'ghassani.adani@pertamina.com', 'user'),
(755340, 'Megaproyek Pengolahan dan Petrokimia', 'Giska Koesumasari Putri', 'giskakoesuma', 'giskakoesuma', 'giska.putri@pertamina.com', 'user'),
(755341, 'Megaproyek Pengolahan dan Petrokimia', 'Hafizh Tandiyanto Putra', 'hafizhtandiy', 'hafizhtandiy', 'hafizh.putra@pertamina.com', 'user'),
(755342, 'Megaproyek Pengolahan dan Petrokimia', 'Hatyo Hadsanggeni', 'hatyohadsang', 'hatyohadsang', 'hatyo.hadsanggeni@pertamina.com', 'user'),
(755343, 'Megaproyek Pengolahan dan Petrokimia', 'Hendra Adiyatma', 'hendraadiyat', 'hendraadiyat', 'hendra.adiyatma@pertamina.com', 'user'),
(755344, 'Megaproyek Pengolahan dan Petrokimia', 'Immanuel Richart Piterson Sembiring', 'immanuelrich', 'immanuelrich', 'immanuel.sembiring@pertamina.com', 'user'),
(755345, 'Megaproyek Pengolahan dan Petrokimia', 'Indra Alexander Tambunan', 'indraalexand', 'indraalexand', 'indra.tambunan@pertamina.com', 'user'),
(755346, 'Megaproyek Pengolahan dan Petrokimia', 'Inshanu Ghalih Wibowo', 'inshanughali', 'inshanughali', 'inshanu.wibowo@pertamina.com', 'user'),
(755347, 'Megaproyek Pengolahan dan Petrokimia', 'Irwan Hanung Septianto', 'irwanhanung', 'irwanhanung1', 'irwan.septianto@pertamina.com', 'user'),
(755348, 'Megaproyek Pengolahan dan Petrokimia', 'Jofie Yananda', 'jofieyananda', 'jofieyananda', 'jofie.yananda@pertamina.com', 'user'),
(755349, 'Megaproyek Pengolahan dan Petrokimia', 'Lee Warren Teguh N', 'leewarren', 'leewarren123', 'lee.warren@pertamina.com', 'user'),
(755350, 'Megaproyek Pengolahan dan Petrokimia', 'Listiani Artha', 'listianiarth', 'listianiarth', 'listiani.artha@pertamina.com', 'user'),
(755351, 'Megaproyek Pengolahan dan Petrokimia', 'Lucky H Puspaningrum', 'luckyh', 'luckyh123', 'lucky.puspaningrum@pertamina.com', 'user'),
(755352, 'Megaproyek Pengolahan dan Petrokimia', 'Muhammad Fauzan Aristyo', 'muhammadfauz', 'muhammadfauz', 'fauzan.aristyo@pertamina.com', 'user'),
(755353, 'Megaproyek Pengolahan dan Petrokimia', 'Maya Prestinawati', 'mayaprestina', 'mayaprestina', 'maya.prestinawati@pertamina.com', 'user'),
(755354, 'Megaproyek Pengolahan dan Petrokimia', 'Minaco Rino', 'minacorino', 'minacorino12', 'minaco.rino@pertamina.com', 'user'),
(755355, 'Megaproyek Pengolahan dan Petrokimia', 'Moch Machrus Adhim', 'mochmachrus', 'mochmachrus1', 'moch.adhim@pertamina.com', 'user'),
(755356, 'Megaproyek Pengolahan dan Petrokimia', 'Muhamad Wahyunda', 'muhamadwahyu', 'muhamadwahyu', 'muhamad.wahyunda@pertamina.com', 'user'),
(755357, 'Megaproyek Pengolahan dan Petrokimia', 'Muhammad Agha Hutama Syukron', 'muhammadagha', 'muhammadagha', 'muhammad.syukron@pertamina.com', 'user'),
(755358, 'Megaproyek Pengolahan dan Petrokimia', 'Muhammad Andre Widianto', 'muhammadandr', 'muhammadandr', 'muhammad.widianto@pertamina.com', 'user'),
(755359, 'Megaproyek Pengolahan dan Petrokimia', 'Muhammad Ardian Nur', 'muhammadardi', 'muhammadardi', 'muhammad.nur2@pertamina.com', 'user'),
(755360, 'Megaproyek Pengolahan dan Petrokimia', 'Muhammad Fajrul Falah Munif', 'muhammadfajr', 'muhammadfajr', 'muhammad.munif@pertamina.com', 'user'),
(755361, 'Megaproyek Pengolahan dan Petrokimia', 'Muhammad Hakim Akbar', 'muhammadhaki', 'muhammadhaki', 'hakim.akbar@pertamina.com', 'user'),
(755362, 'Megaproyek Pengolahan dan Petrokimia', 'Muhammad rizki', 'mrizki', 'mrizki123', 'mr.rizki@pertamina.com', 'user'),
(755363, 'Megaproyek Pengolahan dan Petrokimia', 'Muhammad Rizky', 'muhammadrizk', 'muhammadrizk', 'muh.rizky@pertamina.com', 'user'),
(755364, 'Megaproyek Pengolahan dan Petrokimia', 'Mulia Angara', 'muliaangara', 'muliaangara1', 'mulia.angara@pertamina.com', 'user'),
(755365, 'Megaproyek Pengolahan dan Petrokimia', 'Mulyanisa Nadhifah Sirod', 'mulyanisanad', 'mulyanisanad', 'mulyanisa.sirod@pertamina.com', 'user'),
(755366, 'Megaproyek Pengolahan dan Petrokimia', 'Nadia Rahmeita Prasanti', 'nadiarahmeit', 'nadiarahmeit', 'nadia.prasanti@pertamina.com', 'user'),
(755367, 'Megaproyek Pengolahan dan Petrokimia', 'Naindar Afdanny', 'naindarafdan', 'naindarafdan', 'naindar.afdanny@pertamina.com', 'user'),
(755368, 'Megaproyek Pengolahan dan Petrokimia', 'Natsir Hidayat Pratomo', 'natsirhidaya', 'natsirhidaya', 'natsir.pratomo@pertamina.com', 'user'),
(755369, 'Megaproyek Pengolahan dan Petrokimia', 'Naufal kemal', 'nkemal', 'nkemal123', 'naufal.azmi@pertamina.com', 'user'),
(755370, 'Megaproyek Pengolahan dan Petrokimia', 'Nurjannah Haryanti Putri', 'nurjannahhar', 'nurjannahhar', 'nurjannah.putri@pertamina.com', 'user'),
(755371, 'Megaproyek Pengolahan dan Petrokimia', 'Pryandi Siahaan', 'pryandisiaha', 'pryandisiaha', 'pryandi.siahaan@pertamina.com', 'user'),
(755372, 'Megaproyek Pengolahan dan Petrokimia', 'Rachmat Putra Juniazhar', 'rachmatputra', 'rachmatputra', 'rachmat.juniazhar@pertamina.com', 'user'),
(755373, 'Megaproyek Pengolahan dan Petrokimia', 'Radea Nasri Erfany', 'radeanasri', 'radeanasri12', 'radea.erfany@pertamina.com', 'user'),
(755374, 'Megaproyek Pengolahan dan Petrokimia', 'Rahadian Agnies Septanto Pamungkas', 'rahadianagni', 'rahadianagni', 'rahadian.agnies@pertamina.com', 'user'),
(755375, 'Megaproyek Pengolahan dan Petrokimia', 'Ralang Argi Barus', 'ralangargi', 'ralangargi12', 'ralang.barus@pertamina.com', 'user'),
(755377, 'Megaproyek Pengolahan dan Petrokimia', 'Rinaldy Andhika Putra', 'rinaldyandhi', 'rinaldyandhi', 'rinaldy.putra@pertamina.com', 'user'),
(755379, 'Megaproyek Pengolahan dan Petrokimia', 'Rizky Renanda Nofa', 'rizkyrenanda', 'rizkyrenanda', 'rizky.nofa@pertamina.com', 'user'),
(755380, 'Megaproyek Pengolahan dan Petrokimia', 'Rohmat Hidayat', 'rohmathidaya', 'rohmathidaya', 'rohmat.h@pertamina.com', 'user'),
(755381, 'Megaproyek Pengolahan dan Petrokimia', 'Ryan Aditya Nugraha', 'ryanaditya', 'ryanaditya12', 'ryan.nugraha@pertamina.com', 'user'),
(755382, 'Megaproyek Pengolahan dan Petrokimia', 'Saifullah', 'saifurlah', 'saifurlah123', 'saifullah.s@pertamina.com', 'user'),
(755383, 'Megaproyek Pengolahan dan Petrokimia', 'Satria Dwi Ananda', 'satriadwi', 'satriadwi123', 'satria.ananda@pertamina.com', 'user'),
(755384, 'Megaproyek Pengolahan dan Petrokimia', 'Solehudin', 'solehrdin', 'solehrdin123', 'solehudin@pertamina.com', 'user'),
(755385, 'Megaproyek Pengolahan dan Petrokimia', 'Suprianta Setiawan Putra', 'supriantaset', 'supriantaset', 'suprianta.putra@pertamina.com', 'user'),
(755386, 'Megaproyek Pengolahan dan Petrokimia', 'Tondi H Raja', 'tondih', 'tondih123', 'tondi.raja@pertamina.com', 'user'),
(755387, 'Megaproyek Pengolahan dan Petrokimia', 'Untoro Eko Saputro', 'untoroeko', 'untoroeko123', 'untoro.saputro@pertamina.com', 'user'),
(755388, 'Megaproyek Pengolahan dan Petrokimia', 'Wendy Efendi', 'wendyefendi', 'wendyefendi1', 'wendy.efendi@pertamina.com', 'user'),
(755389, 'Megaproyek Pengolahan dan Petrokimia', 'William Justin Nababan', 'williamjusti', 'williamjusti', 'william.nababan@pertamina.com', 'user'),
(755390, 'Megaproyek Pengolahan dan Petrokimia', 'Wisnu Suryo Jiwandono', 'wisnusuryo', 'wisnusuryo12', 'wisnu.jiwandono@pertamina.com', 'user'),
(755391, 'Megaproyek Pengolahan dan Petrokimia', 'Yollanda Zilviana Devi', 'yollandazilv', 'yollandazilv', 'yollanda.devi@pertamina.com', 'user'),
(755392, 'Megaproyek Pengolahan dan Petrokimia', 'Yulistian Nugraha', 'yulistiannug', 'yulistiannug', 'yulistian@pertamina.com', 'user'),
(755393, 'Megaproyek Pengolahan dan Petrokimia', 'Yusuf Zaelana', 'yusufzaelana', 'yusufzaelana', 'yusuf.zaelana@pertamina.com', 'user'),
(755394, 'Megaproyek Pengolahan dan Petrokimia', 'Zara Karunia Tanjung', 'zarakarunia', 'zarakarunia1', 'zara.tanjung@pertamina.com', 'user'),
(755395, 'Megaproyek Pengolahan dan Petrokimia', 'Zulvikqy Liandy', 'zulvikqylian', 'zulvikqylian', 'zulvikqy.liandy@pertamina.com', 'user'),
(10026611, 'Megaproyek Pengolahan dan Petrokimia', 'Enrico Yandie', 'enricoyandie', 'enricoyandie', 'enrico.yandie@pertamina.com', 'user'),
(10026612, 'Megaproyek Pengolahan dan Petrokimia', 'Dheapati Magonna Tallulembang', 'dheapatimago', 'dheapatimago', 'dheapati.magonna@pertamina.com', 'user'),
(10026614, 'Megaproyek Pengolahan dan Petrokimia', 'Fajar Prasetya Rizkikurniadi', 'fajarprasety', 'fajarprasety', 'fajar.prasetya@pertamina.com', 'user'),
(10026615, 'Megaproyek Pengolahan dan Petrokimia', 'Muhammad Ali Imran', 'muhammadali', 'muhammadali1', 'muhammad.imran@pertamina.com', 'user'),
(10026616, 'Megaproyek Pengolahan dan Petrokimia', 'Dwiki Irvan Mahardika', 'dwikiirvan', 'dwikiirvan12', 'dwiki.irvan@pertamina.com', 'user'),
(10026617, 'Megaproyek Pengolahan dan Petrokimia', 'Achmad Riza Pamula', 'achmadriza', 'achmadriza12', 'achmad.pamula@pertamina.com', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `assessment`
--

CREATE TABLE `assessment` (
  `assessment_id` int(11) NOT NULL,
  `stage_id` int(2) NOT NULL,
  `modul_id` double NOT NULL,
  `employee_id` int(6) NOT NULL,
  `nilai_teori` int(11) NOT NULL,
  `nilai_praktek` int(11) NOT NULL,
  `total_nilai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `modul`
--

CREATE TABLE `modul` (
  `modul_id` double NOT NULL,
  `stage_id` int(2) NOT NULL,
  `brick` int(2) NOT NULL,
  `modul_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modul`
--

INSERT INTO `modul` (`modul_id`, `stage_id`, `brick`, `modul_name`) VALUES
(1.1, 1, 1, 'Work Breakdown Structure'),
(1.2, 1, 1, 'Cost Breakdown Structure'),
(1.3, 1, 1, 'Scoping'),
(1.4, 1, 1, 'Interdependencies'),
(1.5, 1, 1, 'Activity Mapping'),
(1.6, 1, 1, 'Contractor/Vendor Agreement'),
(2.1, 2, 2, 'Concept and Business Case Optimatization'),
(2.2, 2, 2, 'Cost Estimation and Capital Budgeting'),
(2.3, 2, 2, 'Interface Management'),
(2.4, 2, 2, 'Construcability Reviews'),
(2.5, 2, 2, 'Value Engineering Process'),
(2.6, 2, 2, 'Design Change Management'),
(2.7, 2, 2, 'Design Document Management'),
(3.1, 3, 3, 'Project KPIs'),
(3.2, 3, 3, 'Visual Dashboard'),
(3.3, 3, 3, 'Risk Management'),
(3.4, 3, 3, 'Interface Management'),
(4.1, 4, 4, 'High Level Contracting & Procurement Strategy'),
(4.2, 4, 4, 'Detail Contracting & Procurement Strategy'),
(4.3, 4, 4, 'Level of Owner Integration'),
(4.4, 4, 4, 'Constructor Due-Diligence & Prioritization'),
(5.1, 5, 5, 'Contract Structure and T&C'),
(5.2, 5, 5, 'Contract Leverage'),
(5.3, 5, 5, 'Document Control and Correspondence'),
(6.1, 6, 6, 'Crises Responses System'),
(6.2, 6, 6, 'Cross Functional Commissioning Teams'),
(6.3, 6, 6, 'Integrated Commissioning Plans'),
(6.4, 6, 6, 'Commissioning Tools and Systems'),
(6.5, 6, 6, 'Cross Functional Ramp-Up Team');

-- --------------------------------------------------------

--
-- Table structure for table `ruangkerja`
--

CREATE TABLE `ruangkerja` (
  `ruangkerja_id` int(11) NOT NULL,
  `employee_id` int(6) NOT NULL,
  `employee_name` varchar(50) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `start_time` varchar(50) NOT NULL,
  `posttest_score` varchar(20) NOT NULL,
  `status` varchar(15) NOT NULL,
  `deadline_course` varchar(50) NOT NULL,
  `clear_time` varchar(50) NOT NULL,
  `directorate` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruangkerja`
--

INSERT INTO `ruangkerja` (`ruangkerja_id`, `employee_id`, `employee_name`, `user_email`, `start_time`, `posttest_score`, `status`, `deadline_course`, `clear_time`, `directorate`) VALUES
(1, 755303, 'Abdul Karim Amarullah', 'abdul.amarullah@pertamina.com', '2019-06-13 2:21:30', '100', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 10:24:28', 'Megaproyek Pengolahan dan Petrokimia'),
(2, 718027, 'Abdullah', 'abdullah2708@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(3, 750787, 'Achmad Hattary', 'achmad.hattary@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(4, 10026617, 'Achmad Riza Pamula', 'achmad.pamula@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(5, 755304, 'Achmad Syafi\'udin', 'achmad.syafiudin@pertamina.com', '2019-06-12 5:56:43', '73.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 10:44:32', 'Megaproyek Pengolahan dan Petrokimia'),
(6, 714163, 'Ali Dwiyono Haritsyah', 'ad_haritsyah@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(7, 755305, 'Adi Kurnia Muktabar', 'adi.muktabar@pertamina.com', '2019-06-13 6:13:36', '93.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 14:23:25', 'Megaproyek Pengolahan dan Petrokimia'),
(8, 748861, 'Adithya Riswimbardi', 'adithya.riswimbardi@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(9, 755306, 'Adrian Satriaji Wiryawan', 'adrian.wiryawan@pertamina.com', '2019-06-24 12:56:36', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 22:16:03', 'Megaproyek Pengolahan dan Petrokimia'),
(10, 740001, 'Agung Budi Setiono', 'agungb@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(11, 753623, 'Agus Maktum', 'agus.maktum@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(12, 741071, 'Agus Suryono', 'agus.suryono@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(13, 748623, 'Ahmad Aminulloh', 'ahmad.aminulloh@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(14, 755307, 'Ahmad Obrain Ghifari', 'ahmad.ghifari@pertamina.com', '2019-06-19 5:16:46', '100', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 6:40:49', 'Megaproyek Pengolahan dan Petrokimia'),
(15, 740067, 'Albin Ginting', 'albin@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(16, 755308, 'Alfi Al Fahreizy', 'alfi.fahreizy@pertamina.com', '2019-07-05 2:18:29', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 6:26:24', 'Megaproyek Pengolahan dan Petrokimia'),
(17, 755334, 'F Alverina Zagita', 'alverina.zagita@pertamina.com', '2019-06-11 1:31:36', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 13:18:39', 'Megaproyek Pengolahan dan Petrokimia'),
(18, 755310, 'Alvin Murad Rachmadsyah', 'alvin.rachmadsyah@pertamina.com', '2019-06-21 10:17:36', '86.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 15:40:58', 'Megaproyek Pengolahan dan Petrokimia'),
(19, 755311, 'Amalia Dyah Arlita', 'amalia.arlita@pertamina.com', '2019-06-10 3:55:15', '80', 'Cleared', '2019-08-31 16:59:59', '2019-07-20 14:49:33', 'Megaproyek Pengolahan dan Petrokimia'),
(20, 755312, 'Anastasia Ayu Pratiwi', 'anastasia.pratiwi@pertamina.com', '2019-07-03 9:18:37', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-08-01 5:24:50', 'Megaproyek Pengolahan dan Petrokimia'),
(21, 748628, 'Andi Hidayat', 'andi.hidayat@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(22, 755313, 'Andrean Diyandana Filemon', 'andrean.filemon@pertamina.com', '2019-06-10 3:59:23', '80', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 10:59:25', 'Megaproyek Pengolahan dan Petrokimia'),
(23, 748630, 'Anindita Mahendra', 'anindita.mahendra@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(24, 717541, 'Antonius Sianipar', 'antonius.sianipar@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(25, 755314, 'anzhari luthfi', 'anzhari.luthfi@pertamina.com', '2019-06-16 10:28:07', '76.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 8:15:18', 'Megaproyek Pengolahan dan Petrokimia'),
(26, 749811, 'Ardian Eko Handoko', 'ardian.eko@pertamina.com', '2019-09-06 0:39:10', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(27, 708786, 'Ari Dwikoranto', 'ari.dwikoranto@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(28, 706655, 'Arie Wisianto', 'arie.wisianto@pertamina.com', '2019-08-15 7:44:15', '86.6667', 'Cleared', '2019-08-31 16:59:59', '2019-08-27 2:20:27', 'Megaproyek Pengolahan dan Petrokimia'),
(29, 741111, 'Arie Gumilar', 'ariegumilar@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(30, 714593, 'Arif Budiyono', 'arif@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(31, 718554, 'Aris Suparto', 'ariss@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(32, 750094, 'Asep Cahyana', 'asep.cahyana@pertamina.com', '2019-09-13 5:55:26', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(33, 719453, 'Asril Bijaksana', 'asril_b@pertamina.com', '2019-09-09 23:58:12', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(34, 755315, 'Azhar Hudaibie Adhitama', 'azhar.adhitama@pertamina.com', '2019-07-11 14:54:31', '86.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 9:36:01', 'Megaproyek Pengolahan dan Petrokimia'),
(35, 755316, 'Azka Afuza', 'azka.nugroho@pertamina.com', '2019-05-24 7:18:37', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 9:40:47', 'Megaproyek Pengolahan dan Petrokimia'),
(36, 755317, 'Bagaskara Dwi Anugrah', 'bagaskara.anugrah@pertamina.com', '2019-07-07 10:34:00', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 3:29:44', 'Megaproyek Pengolahan dan Petrokimia'),
(37, 755318, 'Bagus Satrio Utomo P', 'bagus.p@pertamina.com', '2019-05-27 15:26:23', '83.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 8:04:53', 'Megaproyek Pengolahan dan Petrokimia'),
(38, 739906, 'Bambang Harimurti', 'bambangh@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(39, 705423, 'Bambang Purwito H.', 'bampur@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(40, 755319, 'Bernando', 'bernando@pertamina.com', '2019-06-20 4:03:02', '93.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 5:00:43', 'Megaproyek Pengolahan dan Petrokimia'),
(41, 755320, 'Bill Qishty Aulia Ardhy Arsya', 'bill.arsya@pertamina.com', '2019-06-18 14:21:23', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 6:38:19', 'Megaproyek Pengolahan dan Petrokimia'),
(42, 755321, 'Bonifacius Raditya Yudha Atmaja', 'bonifacius.atmaja@pertamina.com', '2019-06-11 22:37:02', '83.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 9:07:12', 'Megaproyek Pengolahan dan Petrokimia'),
(43, 717225, 'Budi Anggoro Priyo', 'budi.priyo@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(44, 755331, 'Dwi Budianto', 'budianto.dwi@pertamina.com', '2019-06-19 14:26:02', '76.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-07 3:52:00', 'Megaproyek Pengolahan dan Petrokimia'),
(45, 755322, 'Chaidir Agam', 'chaidir.ubaidillah@pertamina.com', '2019-06-16 12:02:46', '93.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 13:02:53', 'Megaproyek Pengolahan dan Petrokimia'),
(46, 755323, 'Chandra Dewi Rosalina', 'chandra.rosalina@pertamina.com', '2019-06-16 12:11:06', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 9:32:59', 'Megaproyek Pengolahan dan Petrokimia'),
(47, 749814, 'Chandra Ricardo Marulitua', 'chandra.simanjuntak@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(48, 747887, 'Chitra Dewi', 'chitra.dewi@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(49, 719923, 'M. Cholik Fauzi', 'cholik.fauzi@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(50, 755324, 'Cyntia Rachman', 'cyntia.rachman@pertamina.com', '2019-06-16 14:52:59', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-31 11:25:42', 'Megaproyek Pengolahan dan Petrokimia'),
(51, 748635, 'Dadi Ahmad Mawardi', 'dadi.mawardi@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(52, 748087, 'Daniel Romulus', 'daniel.romulus@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(53, 755325, 'Dany Pristiyan', 'dany.pristiyan@pertamina.com', '2019-06-20 3:29:52', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 6:36:05', 'Megaproyek Pengolahan dan Petrokimia'),
(54, 724231, 'Deded Hermawan', 'dededhermawan@pertamina.com', '2019-06-24 7:05:07', '73.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 10:34:47', 'Megaproyek Pengolahan dan Petrokimia'),
(55, 755326, 'Destyawan Saputra', 'destyawan.saputra@pertamina.com', '2019-05-28 1:25:34', '86.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 13:08:07', 'Megaproyek Pengolahan dan Petrokimia'),
(56, 755327, 'Dewanta Priatama', 'dewanta.priatama@pertamina.com', '2019-06-13 9:45:48', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 1:31:01', 'Megaproyek Pengolahan dan Petrokimia'),
(57, 10026612, 'Dheapati Magonna Tallulembang', 'dheapati.magonna@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(58, 750771, 'Dini Septiadhanny', 'dhini.septiadhanny@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(59, 755328, 'Dian Anggraini', 'dian.anggraini@pertamina.com', '2019-06-13 9:49:36', '100', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 7:58:48', 'Megaproyek Pengolahan dan Petrokimia'),
(60, 755330, 'Dionisius Andy Kristanto', 'dionisius.kristanto@pertamina.com', '2019-06-21 12:59:33', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-21 9:03:08', 'Megaproyek Pengolahan dan Petrokimia'),
(61, 718205, 'Djoko Koen Soewito', 'djokokoen@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(62, 741152, 'Dony Satrio Adi', 'dony@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(63, 708218, 'Dwi Wibisana', 'dwibisana@pertamina.com', '2019-07-26 13:52:30', '80', 'Cleared', '2019-08-31 16:59:59', '2019-08-20 23:36:35', 'Megaproyek Pengolahan dan Petrokimia'),
(64, 10026616, 'Dwiki Irvan Mahardika', 'dwiki.irvan@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(65, 709377, 'Dwina Candraasih', 'dwina@pertamina.com', '2019-08-29 21:46:16', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(66, 714739, 'Efrizon', 'efrizon@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(67, 708753, 'Eiman', 'eiman@pertamina.com', '2019-05-31 12:26:52', '76.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-24 14:02:31', 'Megaproyek Pengolahan dan Petrokimia'),
(68, 755332, 'Elmidian Rizky', 'elmidian.rizky@pertamina.com', '2019-06-20 2:04:17', '93.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 0:22:11', 'Megaproyek Pengolahan dan Petrokimia'),
(69, 10026611, 'Enrico Yandie', 'enrico.yandie@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(70, 748639, 'Erick Leonardo', 'erick.butar@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(71, 740091, 'Erwin Thamrin', 'erwin.thamrin@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(72, 747889, 'Ery Gunarto', 'ery.gunarto@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(73, 755333, 'Eviana Dewi Setiawati', 'eviana.setiawati@pertamina.com', '2019-06-13 6:33:44', '86.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 5:03:02', 'Megaproyek Pengolahan dan Petrokimia'),
(74, 10026614, 'Fajar Prasetya Rizkikurniadi', 'fajar.prasetya@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(75, 755336, 'Farida Arisa', 'farida.arisa@pertamina.com', '2019-06-12 10:07:02', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-27 12:04:24', 'Megaproyek Pengolahan dan Petrokimia'),
(76, 755352, 'Muhammad Fauzan Aristyo', 'fauzan.aristyo@pertamina.com', '2019-06-11 9:29:59', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-31 6:57:17', 'Megaproyek Pengolahan dan Petrokimia'),
(77, 748643, 'Febriandi', 'febriandi@pertamina.com', '2019-08-28 20:43:21', '76.6667', 'Cleared', '2019-08-31 16:59:59', '2019-08-29 15:42:20', 'Megaproyek Pengolahan dan Petrokimia'),
(78, 755337, 'Felix Samuel', 'felix.samuel@pertamina.com', '2019-06-14 0:19:05', '100', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 0:20:08', 'Megaproyek Pengolahan dan Petrokimia'),
(79, 730696, 'Rini Fenti Astuti', 'fenti@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(80, 736625, 'Firman Hakim', 'fiha@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(81, 740034, 'I Gusti Bagus Prihanta', 'gbprihanta@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(82, 755338, 'Gema A Firmansyah', 'gema.firmansyah@pertamina.com', '2019-06-26 1:06:22', '83.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 7:47:54', 'Megaproyek Pengolahan dan Petrokimia'),
(83, 755339, 'Ghassani Feta Adani', 'ghassani.adani@pertamina.com', '2019-06-08 3:17:18', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 2:14:49', 'Megaproyek Pengolahan dan Petrokimia'),
(84, 755340, 'Giska Koesumasari Putri', 'giska.putri@pertamina.com', '2019-06-13 0:35:54', '93.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 10:10:55', 'Megaproyek Pengolahan dan Petrokimia'),
(85, 730339, 'Gesit Ngudi Raharjo', 'grahardjo@pertamina.com', '2019-06-16 7:07:04', '86.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 9:57:59', 'Megaproyek Pengolahan dan Petrokimia'),
(86, 755341, 'Hafizh Tandiyanto Putra', 'hafizh.putra@pertamina.com', '2019-05-28 1:52:59', '86.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 1:21:08', 'Megaproyek Pengolahan dan Petrokimia'),
(87, 755361, 'Muhammad Hakim Akbar', 'hakim.akbar@pertamina.com', '2019-06-12 13:11:59', '93.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 3:49:35', 'Megaproyek Pengolahan dan Petrokimia'),
(88, 714682, 'Handi Soejanto Ateng', 'handi@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(89, 746924, 'Hariadi', 'hariadi@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(90, 717688, 'Harmen', 'harmenup2@pertamina.com', '2019-07-29 1:41:40', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 8:04:42', 'Megaproyek Pengolahan dan Petrokimia'),
(91, 755342, 'Hatyo Hadsanggeni', 'hatyo.hadsanggeni@pertamina.com', '2019-06-11 1:11:42', '83.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 2:17:43', 'Megaproyek Pengolahan dan Petrokimia'),
(92, 755343, 'Hendra Adiyatma', 'hendra.adiyatma@pertamina.com', '2019-05-30 5:29:48', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 10:00:02', 'Megaproyek Pengolahan dan Petrokimia'),
(93, 746570, 'Hendrikawan Mulyadi', 'hendrikawan.mulyadi@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(94, 741185, 'Hendro Susanto', 'hendro.susanto@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(95, 748647, 'Hengky', 'hengky@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(96, 748513, 'Hermanto', 'hermanto.sec@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(97, 714569, 'Hery Yudhianto', 'hery_y@pertamina.com', '2019-09-06 22:50:23', '70', 'Cleared', '2019-12-31 16:59:59', '2019-10-28 4:38:49', 'Megaproyek Pengolahan dan Petrokimia'),
(98, 718335, 'Hidayaturrahim', 'hidayaturrahim@pertamina.com', '2019-07-29 3:13:14', '60', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(99, 720124, 'Hendra Onang Wijaya', 'how@pertamina.com', '2019-06-29 14:41:46', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(100, 708201, 'Imam Sunarto', 'i_sunarto@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(101, 708104, 'I Gusti Agung Nyoman Bismayudha', 'igan.bismayudha@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(102, 751674, 'Ika Rosadevi', 'ika.rosadevi@pertamina.com', '2019-07-30 2:00:05', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(103, 746927, 'Iman Sukmajati', 'iman_s@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(104, 755344, 'Immanuel Richart Piterson Sembiring', 'immanuel.sembiring@pertamina.com', '2019-06-19 6:02:42', '86.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 7:30:46', 'Megaproyek Pengolahan dan Petrokimia'),
(105, 746574, 'Imron', 'imron_pe@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(106, 748650, 'Indra Ginanjar', 'indra.ginanjar@pertamina.com', '2019-09-26 0:04:07', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(107, 755345, 'Indra Alexander Tambunan', 'indra.tambunan@pertamina.com', '2019-06-08 3:42:55', '76.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 7:29:34', 'Megaproyek Pengolahan dan Petrokimia'),
(108, 755346, 'Inshanu Ghalih Wibowo', 'inshanu.wibowo@pertamina.com', '2019-06-20 22:01:02', '86.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 13:15:33', 'Megaproyek Pengolahan dan Petrokimia'),
(109, 747533, 'Irfon Wahana Putra', 'irfon.putra@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(110, 755347, 'Irwan Hanung Septianto', 'irwan.septianto@pertamina.com', '2019-06-25 14:44:43', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 23:50:45', 'Megaproyek Pengolahan dan Petrokimia'),
(111, 741233, 'Iwan Priyono', 'iwanpri@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(112, 709214, 'Jadi Purwoko', 'jadi@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(113, 734235, 'James Hengki Mantak', 'jims@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(114, 755348, 'Jofie Yananda', 'jofie.yananda@pertamina.com', '2019-06-26 0:41:36', '73.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 23:52:11', 'Megaproyek Pengolahan dan Petrokimia'),
(115, 747975, 'John Muzibur Simamora', 'john.simamora@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(116, 714796, 'Judy Pudji Tresnacahyono', 'judy@pertamina.com', '2019-09-05 0:26:26', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(117, 713912, 'Khairul Amal', 'k.amal@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(118, 717071, 'Kadek Ambara Jaya', 'kadek@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(119, 726451, 'Kahfi', 'kahfi@pertamina.com', '2019-09-06 7:32:48', '83.3333', 'Cleared', '2019-12-31 16:59:59', '2019-09-16 4:24:14', 'Megaproyek Pengolahan dan Petrokimia'),
(120, 735004, 'H.Kemas A.Johansyah', 'kemas-aj@pertamina.com', '2019-09-06 4:37:13', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(121, 717233, 'Kustinah', 'kustinah@pertamina.com', '2019-06-14 5:49:46', '70', 'Cleared', '2019-08-31 16:59:59', '2019-08-19 8:16:37', 'Megaproyek Pengolahan dan Petrokimia'),
(122, 755349, 'Lee Warren Teguh N', 'lee.warren@pertamina.com', '2019-06-22 10:45:20', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 9:16:23', 'Megaproyek Pengolahan dan Petrokimia'),
(123, 755350, 'Listiani Artha', 'listiani.artha@pertamina.com', '2019-06-14 4:40:10', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 13:25:46', 'Megaproyek Pengolahan dan Petrokimia'),
(124, 755351, 'Lucky H Puspaningrum', 'lucky.puspaningrum@pertamina.com', '2019-06-10 8:12:50', '76.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 14:39:35', 'Megaproyek Pengolahan dan Petrokimia'),
(125, 718498, 'Manson Sihotang', 'manson.sihotang@pertamina.com', '2019-09-11 3:31:40', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(126, 738083, 'Marthen Linggi Pasorong', 'marthen.pasorong@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(127, 755353, 'Maya Prestinawati', 'maya.prestinawati@pertamina.com', '2019-06-11 1:11:38', '93.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 8:33:48', 'Megaproyek Pengolahan dan Petrokimia'),
(128, 713726, 'M.Daud', 'mdaud@pertamina.com', '2019-07-01 23:48:11', '76.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-31 16:19:36', 'Megaproyek Pengolahan dan Petrokimia'),
(129, 755354, 'Minaco Rino', 'minaco.rino@pertamina.com', '2019-06-10 4:03:32', '83.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 14:23:38', 'Megaproyek Pengolahan dan Petrokimia'),
(130, 755355, 'Moch Machrus Adhim', 'moch.adhim@pertamina.com', '2019-06-13 4:35:31', '86.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 8:38:02', 'Megaproyek Pengolahan dan Petrokimia'),
(131, 713848, 'Mochamad Heru Andika', 'mochamad.andika@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(132, 749830, 'Mochamad Nurdiansyah', 'mochamad.nurdiansyah@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(133, 755362, 'Muhammad rizki', 'mr.rizki@pertamina.com', '2019-06-28 6:53:07', '83.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 14:34:59', 'Megaproyek Pengolahan dan Petrokimia'),
(134, 747896, 'Mu\'ti Fahmi', 'mu\'ti.fahmi@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(135, 755363, 'Muhammad Rizky', 'muh.rizky@pertamina.com', '2019-06-19 12:51:10', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 13:26:19', 'Megaproyek Pengolahan dan Petrokimia'),
(136, 755356, 'Muhamad Wahyunda', 'muhamad.wahyunda@pertamina.com', '2019-06-11 9:30:41', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-31 11:58:00', 'Megaproyek Pengolahan dan Petrokimia'),
(137, 10026615, 'Muhammad Ali Imran', 'muhammad.imran@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(138, 755360, 'Muhammad Fajrul Falah Munif', 'muhammad.munif@pertamina.com', '2019-06-26 7:32:03', '83.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 8:47:55', 'Megaproyek Pengolahan dan Petrokimia'),
(139, 755359, 'Muhammad Ardian Nur', 'muhammad.nur2@pertamina.com', '2019-05-30 7:44:57', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 14:30:41', 'Megaproyek Pengolahan dan Petrokimia'),
(140, 748654, 'Muhammad Septiadi Anggoro', 'muhammad.septiadi@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(141, 755357, 'Muhammad Agha Hutama Syukron', 'muhammad.syukron@pertamina.com', '2019-06-20 0:51:53', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 8:25:47', 'Megaproyek Pengolahan dan Petrokimia'),
(142, 755358, 'Muhammad Andre Widianto', 'muhammad.widianto@pertamina.com', '2019-06-13 9:24:58', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 7:23:27', 'Megaproyek Pengolahan dan Petrokimia'),
(143, 755364, 'Mulia Angara', 'mulia.angara@pertamina.com', '2019-06-11 9:29:33', '93.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-31 12:11:29', 'Megaproyek Pengolahan dan Petrokimia'),
(144, 755365, 'Mulyanisa Nadhifah Sirod', 'mulyanisa.sirod@pertamina.com', '2019-06-23 10:18:01', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 12:42:45', 'Megaproyek Pengolahan dan Petrokimia'),
(145, 734851, 'Achmad Muslimin', 'muslimin@pertamina.com', '2019-06-19 2:57:18', '63.3333', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(146, 755366, 'Nadia Rahmeita Prasanti', 'nadia.prasanti@pertamina.com', '2019-06-12 8:21:19', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-26 13:25:24', 'Megaproyek Pengolahan dan Petrokimia'),
(147, 755367, 'Naindar Afdanny', 'naindar.afdanny@pertamina.com', '2019-06-18 1:03:48', '80', 'Cleared', '2019-08-31 16:59:59', '2019-07-21 7:08:14', 'Megaproyek Pengolahan dan Petrokimia'),
(148, 755368, 'Natsir Hidayat Pratomo', 'natsir.pratomo@pertamina.com', '2019-06-11 9:33:46', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-31 9:17:20', 'Megaproyek Pengolahan dan Petrokimia'),
(149, 755369, 'Naufal kemal', 'naufal.azmi@pertamina.com', '2019-06-22 1:14:15', '80', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 22:56:56', 'Megaproyek Pengolahan dan Petrokimia'),
(150, 748694, 'Nur Rochman', 'nur.rochman@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(151, 755370, 'Nurjannah Haryanti Putri', 'nurjannah.putri@pertamina.com', '2019-06-13 9:23:54', '93.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 14:15:00', 'Megaproyek Pengolahan dan Petrokimia'),
(152, 749834, 'Pancar Fransco', 'pancar.fransco@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(153, 717874, 'Pranoto Hutomo', 'pranoto@pertamina.com', '2019-08-31 14:55:49', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(154, 713604, 'Priyo Titi Budhi', 'priyotb@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(155, 755371, 'Pryandi Siahaan', 'pryandi.siahaan@pertamina.com', '2019-06-16 12:54:35', '93.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-31 9:35:07', 'Megaproyek Pengolahan dan Petrokimia'),
(156, 755372, 'Rachmat Putra Juniazhar', 'rachmat.juniazhar@pertamina.com', '2019-05-26 2:42:24', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 5:03:37', 'Megaproyek Pengolahan dan Petrokimia'),
(157, 755373, 'Radea Nasri Erfany', 'radea.erfany@pertamina.com', '2019-06-23 4:05:58', '80', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 11:07:14', 'Megaproyek Pengolahan dan Petrokimia'),
(158, 748660, 'Ragil Priyanto', 'ragil.priyanto@pertamina.com', '2019-09-06 9:29:11', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(159, 755374, 'Rahadian Agnies Septanto Pamungkas', 'rahadian.agnies@pertamina.com', '2019-07-13 17:25:28', '76.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-31 12:07:21', 'Megaproyek Pengolahan dan Petrokimia'),
(160, 708372, 'Rahadi Wicaksono', 'rahadiw@pertamina.com', '2019-08-28 10:07:20', '70', 'Cleared', '2019-08-31 16:59:59', '2019-08-31 12:37:57', 'Megaproyek Pengolahan dan Petrokimia'),
(161, 755375, 'Ralang Argi Barus', 'ralang.barus@pertamina.com', '2019-06-18 0:13:28', '93.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-25 7:24:04', 'Megaproyek Pengolahan dan Petrokimia'),
(162, 748662, 'Rano Karno Sitepu', 'rano.sitepu@pertamina.com', '2019-10-01 11:48:03', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(163, 739088, 'Rekson Junior Rumahorbo', 'rekson@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(164, 747899, 'Rendra Jayantara Putra', 'rendra.jayantara@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(165, 747009, 'ria handayani', 'ria.handayani@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(166, 751882, 'Riani Rahma Vita Sari', 'riani.vita.sari@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(167, 755377, 'Rinaldy Andhika Putra', 'rinaldy.putra@pertamina.com', '2019-06-16 13:07:15', '86.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-16 15:16:51', 'Megaproyek Pengolahan dan Petrokimia'),
(168, 755379, 'Rizky Renanda Nofa', 'rizky.nofa@pertamina.com', '2019-06-13 0:27:24', '83.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 4:28:33', 'Megaproyek Pengolahan dan Petrokimia'),
(169, 719972, 'Roberman Siburian', 'roberman_s@pertamina.com', '2019-09-06 6:44:48', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(170, 739971, 'Rohmadi', 'rohmadi@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(171, 755380, 'Rohmat Hidayat', 'rohmat.h@pertamina.com', '2019-06-14 12:18:05', '83.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 7:24:34', 'Megaproyek Pengolahan dan Petrokimia'),
(172, 731108, 'Rudy Samsulhadi', 'rudys@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(173, 714585, 'Rully Hendarsetiawan', 'rully_h@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(174, 746583, 'Rusmayadi Wardhana', 'rusmayadi@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(175, 755381, 'Ryan Aditya Nugraha', 'ryan.nugraha@pertamina.com', '2019-06-19 1:58:05', '80', 'Cleared', '2019-08-31 16:59:59', '2019-07-27 1:16:28', 'Megaproyek Pengolahan dan Petrokimia'),
(176, 717777, 'Syaifuddin Azhar', 's_azhar@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(177, 708397, 'Sahadi', 'sahadi@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(178, 755382, 'Saifullah', 'saifullah.s@pertamina.com', '2019-06-17 13:55:02', '70', 'Cleared', '2019-08-31 16:59:59', '2019-07-27 7:50:05', 'Megaproyek Pengolahan dan Petrokimia'),
(179, 755383, 'Satria Dwi Ananda', 'satria.ananda@pertamina.com', '2019-06-24 0:33:36', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 0:43:32', 'Megaproyek Pengolahan dan Petrokimia'),
(180, 748666, 'Sayidatul Khoiriyah', 'sayidatul.khoiriyah@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(181, 708097, 'Syamsul Bahri', 'sbahri@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(182, 747547, 'Sigit Heru Susanto', 'sigit.susanto@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(183, 717103, 'Sigit Pradjaka S.', 'sigit_ps@pertamina.com', '2019-09-11 10:36:14', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(184, 714196, 'Soegih Widhodho', 'soegih@pertamina.com', '2019-09-05 8:52:27', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(185, 755384, 'Solehudin', 'solehudin@pertamina.com', '2019-06-11 1:41:32', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 15:37:36', 'Megaproyek Pengolahan dan Petrokimia'),
(186, 751864, 'Srina Ansella', 'srina.ansella@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(187, 734195, 'Sutopo', 'stopo@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(188, 720157, 'Suharta Heru Pujiono', 'suharta@pertamina.com', '2019-09-06 13:38:34', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(189, 739996, 'Sumarno', 'sumarno73@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(190, 755385, 'Suprianta Setiawan Putra', 'suprianta.putra@pertamina.com', '2019-05-30 1:37:38', '80', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 2:14:23', 'Megaproyek Pengolahan dan Petrokimia'),
(191, 718343, 'Supriyono', 'supriyono-ts@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(192, 709044, 'Suwahyanto', 'suwahyanto@pertamina.com', '2019-06-12 12:45:41', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(193, 714666, 'Syarif Hidayat', 'syh4666@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(194, 736293, 'Tedjo Barata Rm', 'tedjob@pertamina.com', '2019-09-12 7:19:36', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(195, 748670, 'Tofan Santoso', 'tofan.santoso@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(196, 755386, 'Tondi H Raja', 'tondi.raja@pertamina.com', '2019-06-30 2:35:15', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-28 7:54:11', 'Megaproyek Pengolahan dan Petrokimia'),
(197, 753535, 'Tri Basoeki Soelis Vichyanto', 'tribasoeki@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(198, 748671, 'Umar Heru Setiadi', 'umar.setiadi@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(199, 755387, 'Untoro Eko Saputro', 'untoro.saputro@pertamina.com', '2019-06-12 9:52:26', '93.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-26 12:44:29', 'Megaproyek Pengolahan dan Petrokimia'),
(200, 714252, 'Usman Hasan', 'usmanh@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(201, 748672, 'Vika Prasetya Andika', 'vika.prasetya@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(202, 714228, 'Waluyo Subiyanto', 'waluyo_s9871@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(203, 755388, 'Wendy Efendi', 'wendy.efendi@pertamina.com', '2019-06-11 1:14:00', '93.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 16:00:00', 'Megaproyek Pengolahan dan Petrokimia'),
(204, 706169, 'Widodo Tri Rahardjo', 'widodo.rahardjo@pertamina.com', '2019-07-30 15:09:00', '86.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-31 15:50:34', 'Megaproyek Pengolahan dan Petrokimia'),
(205, 718149, 'Wiko Taviarto', 'wiko@pertamina.com', '2019-06-27 14:19:58', '100', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 14:59:23', 'Megaproyek Pengolahan dan Petrokimia'),
(206, 755389, 'William Justin Nababan', 'william.nababan@pertamina.com', '2019-06-13 3:52:41', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 16:43:32', 'Megaproyek Pengolahan dan Petrokimia'),
(207, 755390, 'Wisnu Suryo Jiwandono', 'wisnu.jiwandono@pertamina.com', '2019-06-11 1:57:10', '86.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-29 7:13:42', 'Megaproyek Pengolahan dan Petrokimia'),
(208, 717322, 'Herry Yawarto', 'yawarto@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(209, 755391, 'Yollanda Zilviana Devi', 'yollanda.devi@pertamina.com', '2019-06-12 0:15:52', '90', 'Cleared', '2019-08-31 16:59:59', '2019-07-31 0:20:01', 'Megaproyek Pengolahan dan Petrokimia'),
(210, 749845, 'Yoppi Sutrisna', 'yoppi.sutrisna@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(211, 718368, 'Yosep Asro Wain', 'yosep.wain@pertamina.com', '2019-06-23 11:18:43', '80', 'Cleared', '2019-08-31 16:59:59', '2019-07-31 9:13:22', 'Megaproyek Pengolahan dan Petrokimia'),
(212, 717063, 'Yoyok Poerwedi', 'ypoerwedi@pertamina.com', '2019-09-05 2:43:43', '0', 'Ongoing', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(213, 755286, 'Yudha Prastyono', 'yudha.prastyono@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(214, 714341, 'Yuherwin', 'yuherwin@pertamina.com', 'None', '0', 'Not Started', '2019-12-31 16:59:59', 'None', 'Megaproyek Pengolahan dan Petrokimia'),
(215, 755392, 'Yulistian Nugraha', 'yulistian@pertamina.com', '2019-06-13 9:24:54', '93.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 6:59:19', 'Megaproyek Pengolahan dan Petrokimia'),
(216, 755393, 'Yusuf Zaelana', 'yusuf.zaelana@pertamina.com', '2019-05-27 3:04:16', '86.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-27 0:42:55', 'Megaproyek Pengolahan dan Petrokimia'),
(217, 755394, 'Zara Karunia Tanjung', 'zara.tanjung@pertamina.com', '2019-06-27 13:20:43', '96.6667', 'Cleared', '2019-08-31 16:59:59', '2019-07-27 12:48:22', 'Megaproyek Pengolahan dan Petrokimia'),
(218, 755395, 'Zulvikqy Liandy', 'zulvikqy.liandy@pertamina.com', '2019-06-20 2:56:48', '93.3333', 'Cleared', '2019-08-31 16:59:59', '2019-07-30 7:01:37', 'Megaproyek Pengolahan dan Petrokimia');

-- --------------------------------------------------------

--
-- Table structure for table `stage`
--

CREATE TABLE `stage` (
  `stage_id` int(11) NOT NULL,
  `stage_name` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stage`
--

INSERT INTO `stage` (`stage_id`, `stage_name`) VALUES
(1, 'Stage 1'),
(2, 'Stage 2'),
(3, 'Stage 3');

-- --------------------------------------------------------

--
-- Table structure for table `workbook`
--

CREATE TABLE `workbook` (
  `workbook_id` int(11) NOT NULL,
  `stage_id` int(2) NOT NULL,
  `modul_id` double NOT NULL,
  `employee_id` int(6) NOT NULL,
  `employee_name` varchar(50) NOT NULL,
  `progress` text NOT NULL,
  `status` enum('BELUM','PROSES','SELESAI') NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workbook`
--

INSERT INTO `workbook` (`workbook_id`, `stage_id`, `modul_id`, `employee_id`, `employee_name`, `progress`, `status`, `image`) VALUES
(8, 3, 1.1, 755356, 'Muhamad Wahyunda', 'Belum mengerjakan sama sekali', 'SELESAI', '61.jpg'),
(26, 3, 1.1, 755356, 'Rizal', 'Belum mengerjakan sama sekali', 'BELUM', 'tes'),
(27, 3, 1.1, 755356, 'Rizal', 'Belum mengerjakan sama sekali', 'BELUM', '.png'),
(29, 3, 1.1, 755356, 'Rizal', 'Belum mengerjakan sama sekali', 'BELUM', '.png'),
(30, 3, 1.1, 755356, 'Rizal', 'Belum mengerjakan sama sekali', 'BELUM', '.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `assessment`
--
ALTER TABLE `assessment`
  ADD PRIMARY KEY (`assessment_id`),
  ADD KEY `stage_id` (`stage_id`),
  ADD KEY `modul_id` (`modul_id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- Indexes for table `modul`
--
ALTER TABLE `modul`
  ADD PRIMARY KEY (`modul_id`),
  ADD KEY `stage_id` (`stage_id`);

--
-- Indexes for table `ruangkerja`
--
ALTER TABLE `ruangkerja`
  ADD PRIMARY KEY (`ruangkerja_id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `employee_id_2` (`employee_id`),
  ADD KEY `employee_id_3` (`employee_id`);

--
-- Indexes for table `stage`
--
ALTER TABLE `stage`
  ADD PRIMARY KEY (`stage_id`);

--
-- Indexes for table `workbook`
--
ALTER TABLE `workbook`
  ADD PRIMARY KEY (`workbook_id`),
  ADD KEY `stage_id` (`stage_id`),
  ADD KEY `modul_id` (`modul_id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ruangkerja`
--
ALTER TABLE `ruangkerja`
  MODIFY `ruangkerja_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;

--
-- AUTO_INCREMENT for table `workbook`
--
ALTER TABLE `workbook`
  MODIFY `workbook_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assessment`
--
ALTER TABLE `assessment`
  ADD CONSTRAINT `assessment_ibfk_1` FOREIGN KEY (`stage_id`) REFERENCES `stage` (`stage_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assessment_ibfk_2` FOREIGN KEY (`modul_id`) REFERENCES `modul` (`modul_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assessment_ibfk_3` FOREIGN KEY (`employee_id`) REFERENCES `account` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `workbook`
--
ALTER TABLE `workbook`
  ADD CONSTRAINT `workbook_ibfk_1` FOREIGN KEY (`stage_id`) REFERENCES `stage` (`stage_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `workbook_ibfk_2` FOREIGN KEY (`modul_id`) REFERENCES `modul` (`modul_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `workbook_ibfk_3` FOREIGN KEY (`employee_id`) REFERENCES `account` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
